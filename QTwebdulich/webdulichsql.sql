USE [master]
GO
/****** Object:  Database [webdulich]    Script Date: 7/12/2020 7:50:24 PM ******/
CREATE DATABASE [webdulich]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'webdulich', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\webdulich.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'webdulich_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\webdulich_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [webdulich] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [webdulich].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [webdulich] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [webdulich] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [webdulich] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [webdulich] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [webdulich] SET ARITHABORT OFF 
GO
ALTER DATABASE [webdulich] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [webdulich] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [webdulich] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [webdulich] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [webdulich] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [webdulich] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [webdulich] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [webdulich] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [webdulich] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [webdulich] SET  DISABLE_BROKER 
GO
ALTER DATABASE [webdulich] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [webdulich] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [webdulich] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [webdulich] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [webdulich] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [webdulich] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [webdulich] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [webdulich] SET RECOVERY FULL 
GO
ALTER DATABASE [webdulich] SET  MULTI_USER 
GO
ALTER DATABASE [webdulich] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [webdulich] SET DB_CHAINING OFF 
GO
ALTER DATABASE [webdulich] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [webdulich] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [webdulich] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'webdulich', N'ON'
GO
ALTER DATABASE [webdulich] SET QUERY_STORE = OFF
GO
USE [webdulich]
GO
/****** Object:  Table [dbo].[banner]    Script Date: 7/12/2020 7:50:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[banner](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ten] [nvarchar](200) NULL,
	[ngaytao] [datetime] NULL,
	[ngaycapnhat] [datetime] NULL,
	[daxoa] [bit] NULL,
	[active] [bit] NULL,
	[nguoitao] [int] NULL,
	[hinhanh] [nvarchar](200) NULL,
	[loai] [int] NULL,
 CONSTRAINT [PK_banner] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[chitietdattour]    Script Date: 7/12/2020 7:50:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chitietdattour](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[matour] [int] NULL,
	[makhach] [int] NULL,
	[yeucau] [nvarchar](max) NULL,
	[hoten] [nvarchar](200) NULL,
	[dienthoai] [nvarchar](20) NULL,
	[email] [nvarchar](50) NULL,
	[ngaytao] [datetime] NULL,
	[ngaycapnhat] [datetime] NULL,
	[hinhthucthanhtoan] [int] NULL,
	[giatien] [int] NULL,
	[giamgia] [int] NULL,
	[daxoa] [bit] NULL,
	[tinhtrangdonhang] [int] NULL,
 CONSTRAINT [PK_chitietdattour] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[diadiem]    Script Date: 7/12/2020 7:50:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[diadiem](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ten] [nvarchar](200) NULL,
	[nguoitao] [int] NULL,
	[daxoa] [bit] NULL,
	[ngaytao] [datetime] NULL,
	[ngaycapnhat] [datetime] NULL,
 CONSTRAINT [PK_place] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[khachhang]    Script Date: 7/12/2020 7:50:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[khachhang](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ten] [nvarchar](200) NULL,
	[dienthoai] [nvarchar](20) NULL,
	[email] [nvarchar](50) NULL,
	[username] [nvarchar](20) NULL,
	[password] [nvarchar](20) NULL,
	[ngaytao] [datetime] NULL,
	[ngaycapnhat] [datetime] NULL,
	[daxoa] [bit] NULL,
	[active] [bit] NULL,
 CONSTRAINT [PK_khachhang] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lienhe]    Script Date: 7/12/2020 7:50:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lienhe](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[hoten] [nvarchar](200) NULL,
	[diachi] [ntext] NULL,
	[email] [nvarchar](500) NULL,
	[phone] [varchar](20) NULL,
	[tieude] [nvarchar](100) NULL,
	[yeucau] [ntext] NULL,
	[ngaytao] [datetime] NULL,
 CONSTRAINT [PK_lienhe] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[phuongtien]    Script Date: 7/12/2020 7:50:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[phuongtien](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ten] [nvarchar](200) NULL,
	[nguoitao] [int] NULL,
	[daxoa] [bit] NULL,
	[ngaytao] [datetime] NULL,
	[ngaycapnhat] [datetime] NULL,
 CONSTRAINT [PK_vehicle] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[taikhoan]    Script Date: 7/12/2020 7:50:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taikhoan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[ngaytao] [datetime] NULL,
	[landangnhapcuoi] [datetime] NULL,
	[hoten] [nvarchar](255) NULL,
	[gioitinh] [bit] NULL,
	[ghichu] [ntext] NULL,
	[quyen] [int] NULL,
	[daxoa] [bit] NULL,
 CONSTRAINT [PK_admin_account] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tour]    Script Date: 7/12/2020 7:50:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tour](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ten] [nvarchar](200) NULL,
	[hinhanh] [nvarchar](200) NULL,
	[thoigian] [datetime] NULL,
	[gia] [int] NULL,
	[tomtat] [nvarchar](max) NULL,
	[chitiet] [nvarchar](max) NULL,
	[matour] [nvarchar](20) NULL,
	[phuongtien_fk] [int] NULL,
	[hienthi] [bit] NULL,
	[noibat] [bit] NULL,
	[nguoitao_fk] [int] NULL,
	[diadiem_fk] [int] NULL,
	[daxoa] [bit] NULL,
	[ngaytao] [datetime] NULL,
	[ngaycapnhat] [datetime] NULL,
	[giamgia] [int] NULL,
 CONSTRAINT [PK_tb_tour] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[banner] ON 

INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1003, N'banne lien he', CAST(N'2020-03-15T00:00:00.000' AS DateTime), CAST(N'2020-04-19T16:48:31.660' AS DateTime), 1, 1, 1, N'/UserUpload/images/hinhtour1.jpg', 2)
INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1004, N'banne lien he', CAST(N'2020-03-15T00:00:00.000' AS DateTime), CAST(N'2020-04-19T16:48:34.543' AS DateTime), 1, 1, 1, N'/UserUpload/images/hinhtour1.jpg', 2)
INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1005, N'aaa', CAST(N'2020-03-15T00:00:00.000' AS DateTime), CAST(N'2020-04-19T16:48:41.097' AS DateTime), 1, 1, 1, N'/UserUpload/images/1.jpg', 3)
INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1006, N'fff', CAST(N'2020-03-15T00:00:00.000' AS DateTime), CAST(N'2020-03-15T00:00:00.000' AS DateTime), 1, 1, 1, N'/UserUpload/images/hinhtour1.jpg', 4)
INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1007, N'banner trang chu ', CAST(N'2020-04-19T16:35:00.110' AS DateTime), CAST(N'2020-04-19T16:48:46.660' AS DateTime), 1, 1, 1, N'/UserUpload/images/1.jpg', 4)
INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1008, N'fffooo', CAST(N'2020-04-19T16:35:45.973' AS DateTime), CAST(N'2020-04-19T16:48:49.680' AS DateTime), 1, NULL, 1, N'/UserUpload/images/1.jpg', 4)
INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1009, N'Mpllll', CAST(N'2020-04-19T16:38:25.577' AS DateTime), CAST(N'2020-04-19T16:48:52.703' AS DateTime), 1, NULL, 1, N'/UserUpload/images/hinhvuong.jpg', 4)
INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1010, N'banner trang chu ', CAST(N'2020-04-19T16:40:49.000' AS DateTime), CAST(N'2020-04-19T16:48:20.340' AS DateTime), 1, 0, 1, N'/UserUpload/images/hinh2.jpg', 1)
INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1011, N'banner trang chu ', CAST(N'2020-04-19T16:49:08.000' AS DateTime), CAST(N'2020-04-19T16:50:24.483' AS DateTime), 1, 1, 1, N'/UserUpload/images/hinhtour1.jpg', 1)
INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1012, N'banner trang chu ', CAST(N'2020-06-17T18:42:50.000' AS DateTime), CAST(N'2020-06-17T18:46:09.733' AS DateTime), 0, 0, 1, N'/UserUpload/images/1(1).jpg', 1)
INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1013, N'banner tour', CAST(N'2020-06-17T18:43:39.533' AS DateTime), CAST(N'2020-06-17T18:43:39.533' AS DateTime), 0, 1, 1, N'/UserUpload/images/2.jpg', 2)
INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1014, N'banner tour', CAST(N'2020-06-17T18:44:40.373' AS DateTime), CAST(N'2020-06-17T18:44:40.373' AS DateTime), 0, 1, 1, N'/UserUpload/images/3.jpg', 3)
INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1015, N'banner trang chu ', CAST(N'2020-06-17T18:45:23.000' AS DateTime), CAST(N'2020-06-17T18:46:03.027' AS DateTime), 0, 1, 1, N'/UserUpload/images/aaaaaaa.jpg', 1)
INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1016, N'banner CTT', CAST(N'2020-06-17T18:46:46.503' AS DateTime), CAST(N'2020-06-17T18:58:46.983' AS DateTime), 1, 1, 1, N'/UserUpload/images/4.jpg', 4)
INSERT [dbo].[banner] ([id], [ten], [ngaytao], [ngaycapnhat], [daxoa], [active], [nguoitao], [hinhanh], [loai]) VALUES (1017, N'banner', CAST(N'2020-06-17T18:59:06.000' AS DateTime), CAST(N'2020-07-02T00:38:23.057' AS DateTime), 0, 1, 1, N'/UserUpload/images/2.jpg', 4)
SET IDENTITY_INSERT [dbo].[banner] OFF
SET IDENTITY_INSERT [dbo].[chitietdattour] ON 

INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (10, 4, NULL, N'yeu cau 2', N'Nguyen Van B', N'0901115565', N'nguyenvanb@gmail.com', CAST(N'2020-06-14T14:15:59.000' AS DateTime), CAST(N'2020-06-14T14:15:59.000' AS DateTime), 2, 1000000, 25, NULL, 1)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (11, 6, NULL, N'yeu cau 3', N'Nguyen Van C', N'0901115563', N'nguyenvanc@gmail.com', CAST(N'2020-06-14T14:16:26.000' AS DateTime), CAST(N'2020-06-14T14:16:26.000' AS DateTime), 1, 150000, 15, NULL, 2)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (12, 1003, 4, N'yeu cau 123', N'Trần Quang Tùng', N'0329489889', N'tranquangtung2502@gmail.com', CAST(N'2020-07-02T18:59:52.643' AS DateTime), CAST(N'2020-07-02T18:59:52.643' AS DateTime), 1, 6900000, 10, 0, 1)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (13, 1005, 0, N'đạiioas', N'Nguyen Van manh', N'0901115564', N'doibantay97@gmail.com', CAST(N'2020-07-03T10:40:45.000' AS DateTime), CAST(N'2020-07-03T10:40:45.000' AS DateTime), 1, 5000000, 20, 0, 3)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (14, 1005, 4, N'cjca', N'Trần Quang Tùng', N'0329489889', N'tranquangtung2502@gmail.com', CAST(N'2020-07-03T10:42:20.763' AS DateTime), CAST(N'2020-07-04T18:21:57.100' AS DateTime), 2, 5000000, 20, 0, 4)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (15, 1004, NULL, N'yeu cau abc', N'khach hang 1', N'0901115565', N'hunganhly2208@gmail.com', CAST(N'2020-07-12T17:44:40.960' AS DateTime), CAST(N'2020-07-11T17:44:40.960' AS DateTime), 1, 5900000, 15, 0, 1)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (16, 1004, NULL, N'zxc zxc', N'Trần Tùng 33', N'0901115564', N'tranquangtung9889@gmail.com', CAST(N'2020-07-12T18:07:49.493' AS DateTime), CAST(N'2020-07-11T18:07:49.493' AS DateTime), 1, 5900000, 15, 0, 1)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (17, 1005, NULL, N'xc asd', N'Trần Tùng', N'0901115564', N'tranquangtung9889@gmail.com', CAST(N'2020-07-12T18:09:47.380' AS DateTime), CAST(N'2020-07-11T18:09:47.380' AS DateTime), 1, 5000000, 20, 0, 1)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (18, 1006, NULL, N'qew qwe', N'Trần Tùng', N'0901115564', N'tranquangtung9889@gmail.com', CAST(N'2020-07-12T18:09:47.380' AS DateTime), CAST(N'2020-07-11T18:16:20.840' AS DateTime), 1, 1600000, 19, 0, 1)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (19, 1004, NULL, N'qwe eqw', N'Trần Tùng 33', N'0901115565', N'tranquangtung9889@gmail.com', CAST(N'2020-07-12T18:09:47.380' AS DateTime), CAST(N'2020-07-11T18:20:39.107' AS DateTime), 1, 5900000, 15, 0, 1)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (20, 1004, NULL, N'qwe eqw', N'Trần Tùng', N'0901115564', N'hunganhly2208@gmail.com', CAST(N'2020-07-12T18:09:47.380' AS DateTime), CAST(N'2020-07-11T18:21:38.560' AS DateTime), 1, 5900000, 15, 0, 1)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (21, 1004, NULL, N'zxc zx', N'Trần Tùngqwe', N'0901115564', N'hunganhly2208@gmail.com', CAST(N'2020-07-12T18:09:47.380' AS DateTime), CAST(N'2020-07-11T18:23:57.373' AS DateTime), 1, 5900000, 15, 0, 1)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (22, 1005, NULL, N'zd qwe', N'Trần Tùng', N'0901115565', N'tranquangtung9889@gmail.com', CAST(N'2020-07-12T18:09:47.380' AS DateTime), CAST(N'2020-07-11T18:25:35.427' AS DateTime), 1, 5000000, 20, 0, 1)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (23, 1005, 0, N'asd qwe', N'Trần Tùng 33', N'0901115565', N'hunganhly2208@gmail.com', CAST(N'2020-07-12T18:09:47.000' AS DateTime), CAST(N'2020-07-11T18:31:02.000' AS DateTime), 1, 5000000, 20, 0, 4)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (24, 1005, NULL, N'asd qwe', N'Trần Tùng', N'0901115565', N'hunganhly2208@gmail.com', CAST(N'2020-07-12T18:09:47.380' AS DateTime), CAST(N'2020-07-11T18:33:22.660' AS DateTime), 1, 5000000, 20, 0, 1)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (25, 1005, NULL, N'asd qwe', N'Trần Tùng', N'0901115565', N'hunganhly2208@gmail.com', CAST(N'2020-07-12T18:09:47.380' AS DateTime), CAST(N'2020-07-11T18:34:31.567' AS DateTime), 1, 5000000, 20, 0, 1)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (26, 1003, 0, N'123 asd', N'Trần Tùng 33', N'0901115565', N'hunganhly2208@gmail.com', CAST(N'2020-07-12T18:09:47.000' AS DateTime), CAST(N'2020-07-11T18:35:24.000' AS DateTime), 1, 6900000, 10, 0, 3)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (27, 1003, NULL, N'asd wqe', N'Trần Tùng', N'0901115565', N'hunganhly2208@gmail.com', CAST(N'2020-07-12T18:09:47.380' AS DateTime), CAST(N'2020-07-11T18:36:53.550' AS DateTime), 1, 6900000, 10, 0, 1)
INSERT [dbo].[chitietdattour] ([id], [matour], [makhach], [yeucau], [hoten], [dienthoai], [email], [ngaytao], [ngaycapnhat], [hinhthucthanhtoan], [giatien], [giamgia], [daxoa], [tinhtrangdonhang]) VALUES (28, 1003, NULL, N'asd qwe', N'Trần Tùng', N'0901115565', N'hunganhly2208@gmail.com', CAST(N'2020-07-12T18:09:47.380' AS DateTime), CAST(N'2020-07-11T18:38:52.827' AS DateTime), 1, 6900000, 10, 0, 1)
SET IDENTITY_INSERT [dbo].[chitietdattour] OFF
SET IDENTITY_INSERT [dbo].[diadiem] ON 

INSERT [dbo].[diadiem] ([id], [ten], [nguoitao], [daxoa], [ngaytao], [ngaycapnhat]) VALUES (1, N'Nha Trang - Khanh Hoa', 1, 0, CAST(N'2020-03-15T00:00:00.000' AS DateTime), CAST(N'2020-03-15T00:00:00.000' AS DateTime))
INSERT [dbo].[diadiem] ([id], [ten], [nguoitao], [daxoa], [ngaytao], [ngaycapnhat]) VALUES (2, N'Pleiku - Kom Tum - Buôn Ma Thuật', 1, NULL, CAST(N'2020-03-15T00:00:00.000' AS DateTime), CAST(N'2020-03-15T00:00:00.000' AS DateTime))
INSERT [dbo].[diadiem] ([id], [ten], [nguoitao], [daxoa], [ngaytao], [ngaycapnhat]) VALUES (3, N'Quy Nhơn - Kì Co', 1, NULL, CAST(N'2020-06-15T00:00:00.000' AS DateTime), CAST(N'2020-06-15T00:00:00.000' AS DateTime))
INSERT [dbo].[diadiem] ([id], [ten], [nguoitao], [daxoa], [ngaytao], [ngaycapnhat]) VALUES (4, N'Đà Lạt Tour', 1, NULL, CAST(N'2020-06-15T00:00:00.000' AS DateTime), CAST(N'2020-06-15T00:00:00.000' AS DateTime))
INSERT [dbo].[diadiem] ([id], [ten], [nguoitao], [daxoa], [ngaytao], [ngaycapnhat]) VALUES (5, N'Đà Nẵng -  Hội An', 1, NULL, NULL, NULL)
INSERT [dbo].[diadiem] ([id], [ten], [nguoitao], [daxoa], [ngaytao], [ngaycapnhat]) VALUES (6, N'Hà Nội - Yên Tử - Hạ Long', 1, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[diadiem] OFF
SET IDENTITY_INSERT [dbo].[khachhang] ON 

INSERT [dbo].[khachhang] ([id], [ten], [dienthoai], [email], [username], [password], [ngaytao], [ngaycapnhat], [daxoa], [active]) VALUES (0, N'Nguyen Van A', N'02022211115', N'doibanchan97@gmail.com', N'user01', N'123465', CAST(N'2020-06-27T10:25:28.880' AS DateTime), CAST(N'2020-06-27T10:25:29.357' AS DateTime), 0, 1)
INSERT [dbo].[khachhang] ([id], [ten], [dienthoai], [email], [username], [password], [ngaytao], [ngaycapnhat], [daxoa], [active]) VALUES (1, N'zxc', N'0123456789', N'asad@gmail.com', N'user02', N'123456', CAST(N'2020-07-01T23:26:22.973' AS DateTime), CAST(N'2020-07-01T23:26:23.350' AS DateTime), 0, 1)
INSERT [dbo].[khachhang] ([id], [ten], [dienthoai], [email], [username], [password], [ngaytao], [ngaycapnhat], [daxoa], [active]) VALUES (3, N'qq ww', N'0123456777', N'zzss@gmail.com', N'user011', N'123465', CAST(N'2020-07-02T00:16:19.233' AS DateTime), CAST(N'2020-07-02T14:08:41.443' AS DateTime), 1, 1)
INSERT [dbo].[khachhang] ([id], [ten], [dienthoai], [email], [username], [password], [ngaytao], [ngaycapnhat], [daxoa], [active]) VALUES (4, N'Trần Quang Tùng ', N'0329489889', N'tranquangtung2502@gmail.com', N'quangtung9889', N'123456', CAST(N'2020-07-02T13:57:51.170' AS DateTime), CAST(N'2020-07-06T23:41:17.300' AS DateTime), 0, 1)
INSERT [dbo].[khachhang] ([id], [ten], [dienthoai], [email], [username], [password], [ngaytao], [ngaycapnhat], [daxoa], [active]) VALUES (5, N'tùng', N'032948988', N'zxc@gmail.com', N'quangtung98899', N'123456', CAST(N'2020-07-02T13:58:35.767' AS DateTime), CAST(N'2020-07-02T13:58:35.767' AS DateTime), 0, 1)
INSERT [dbo].[khachhang] ([id], [ten], [dienthoai], [email], [username], [password], [ngaytao], [ngaycapnhat], [daxoa], [active]) VALUES (6, N'Nguyên văn cường', N'0901115563', N'nguyenvanc@gmail.com', N'manh123', N'123', CAST(N'2020-07-03T10:44:23.100' AS DateTime), CAST(N'2020-07-03T10:44:23.100' AS DateTime), 0, 1)
SET IDENTITY_INSERT [dbo].[khachhang] OFF
SET IDENTITY_INSERT [dbo].[lienhe] ON 

INSERT [dbo].[lienhe] ([id], [hoten], [diachi], [email], [phone], [tieude], [yeucau], [ngaytao]) VALUES (1, N'Trần Tùng', N'p14 quan go vap', N'doibantay97@gmail.com', N'0123444555', N'asd', N'asd zxc', CAST(N'2020-07-12T19:17:24.970' AS DateTime))
INSERT [dbo].[lienhe] ([id], [hoten], [diachi], [email], [phone], [tieude], [yeucau], [ngaytao]) VALUES (2, N'Trần Tùng 33', N'p14 quan go vap', N'doibantay97@gmail.com', N'0123444555', N'asd', N'adqwe qe qwe rewr', CAST(N'2020-07-12T19:18:36.217' AS DateTime))
INSERT [dbo].[lienhe] ([id], [hoten], [diachi], [email], [phone], [tieude], [yeucau], [ngaytao]) VALUES (3, N'Trần Tùng 33', N'p14 quan go vap', N'doibanchan97@gmail.com', N'0123444555', N'ggvhgvv', N'cf ggj', CAST(N'2020-07-12T19:22:40.720' AS DateTime))
INSERT [dbo].[lienhe] ([id], [hoten], [diachi], [email], [phone], [tieude], [yeucau], [ngaytao]) VALUES (4, N'tran quang tung', N'p14 quan go vap', N'doibantay97@gmail.com', N'0123444555', N'sdcsc', N'dsvdfsv fvavd', CAST(N'2020-07-12T19:25:00.333' AS DateTime))
SET IDENTITY_INSERT [dbo].[lienhe] OFF
SET IDENTITY_INSERT [dbo].[phuongtien] ON 

INSERT [dbo].[phuongtien] ([id], [ten], [nguoitao], [daxoa], [ngaytao], [ngaycapnhat]) VALUES (1, N'Máy bay', 1, 0, CAST(N'2020-03-15T00:00:00.000' AS DateTime), CAST(N'2020-03-15T00:00:00.000' AS DateTime))
INSERT [dbo].[phuongtien] ([id], [ten], [nguoitao], [daxoa], [ngaytao], [ngaycapnhat]) VALUES (2, N'Xe theo tour', 1, 0, CAST(N'2020-03-15T00:00:00.000' AS DateTime), CAST(N'2020-03-15T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[phuongtien] OFF
SET IDENTITY_INSERT [dbo].[taikhoan] ON 

INSERT [dbo].[taikhoan] ([id], [username], [password], [ngaytao], [landangnhapcuoi], [hoten], [gioitinh], [ghichu], [quyen], [daxoa]) VALUES (1, N'admin', N'123', CAST(N'2020-03-15T00:00:00.000' AS DateTime), CAST(N'2020-07-12T19:15:19.710' AS DateTime), N'tran quang tung', 1, N'ad', 1, 0)
SET IDENTITY_INSERT [dbo].[taikhoan] OFF
SET IDENTITY_INSERT [dbo].[tour] ON 

INSERT [dbo].[tour] ([id], [ten], [hinhanh], [thoigian], [gia], [tomtat], [chitiet], [matour], [phuongtien_fk], [hienthi], [noibat], [nguoitao_fk], [diadiem_fk], [daxoa], [ngaytao], [ngaycapnhat], [giamgia]) VALUES (4, N'tou11', N'/UserUpload/images/hinhvuong.jpg', CAST(N'2020-11-11T00:00:00.000' AS DateTime), 1000000, N'tomatat tour1', N'<p>chitiet tour1</p>
', N'tou001', 1, 1, 0, 1, 1, 1, CAST(N'2020-03-15T00:00:00.000' AS DateTime), CAST(N'2020-03-15T00:00:00.000' AS DateTime), 25)
INSERT [dbo].[tour] ([id], [ten], [hinhanh], [thoigian], [gia], [tomtat], [chitiet], [matour], [phuongtien_fk], [hienthi], [noibat], [nguoitao_fk], [diadiem_fk], [daxoa], [ngaytao], [ngaycapnhat], [giamgia]) VALUES (6, N'Tour Nửa Ngày Làng Bích Họa Tam Thanh từ Đà Nẵng dành cho 2 người', N'/UserUpload/images/hinhtour1.jpg', CAST(N'2020-11-11T00:00:00.000' AS DateTime), 150000, N'Tour Nửa Ngày Làng Bích Họa Tam Thanh từ Đà Nẵng', N'<div>
<p>Tham gia tour du lịch trong ng&agrave;y đến th&agrave;nh phố cổ k&iacute;nh Tam Kỳ, một trong những điểm du lịch mới của Việt Nam gần đ&acirc;y đ&atilde; được biến th&agrave;nh ph&ograve;ng trưng b&agrave;y sinh hoạt văn h&oacute;a v&agrave; cộng đồng địa phương của đất nước. Từ Đ&agrave; Nẵng, xe đưa đ&oacute;n sẽ đưa bạn đến với Tượng Mẹ Việt Nam Anh H&ugrave;ng, để tưởng nhớ đến c&aacute;c b&agrave; mẹ Việt Nam đ&atilde; mất con v&agrave; ch&aacute;u của họ trong c&aacute;c cuộc chiến tranh lịch sử. Quan s&aacute;t tượng cao 18,6m v&agrave; rộng 120m được x&acirc;y dựng tr&ecirc;n diện t&iacute;ch 15 h&eacute;cta v&agrave; chụp một v&agrave;i bức ảnh để mang về nh&agrave; l&agrave;m qu&agrave; lưu niệm trước khi bạn đi đến điểm dừng ch&acirc;n tiếp theo v&agrave; điểm nổi bật của tour du lịch - L&agrave;ng B&iacute;ch Họa Tam Thanh. Được khởi xướng bởi Hội giao lưu nghệ thuật cộng đồng H&agrave;n Quốc v&agrave; Ủy ban nh&acirc;n d&acirc;n tỉnh Quảng Nam, việc biến chuyển l&agrave;ng ch&agrave;i truyền thống l&agrave; một phần của phong tr&agrave;o l&agrave;m cho nghệ thuật c&oacute; thể tiếp cận được với tất cả nền tảng kinh tế x&atilde; hội. Chỉ trong hai tuần, ng&ocirc;i l&agrave;ng Việt Nam đ&atilde; được biến chuyển th&agrave;nh một thi&ecirc;n đường vẽ tranh tường của c&aacute;c nghệ sĩ v&agrave; t&igrave;nh nguyện vi&ecirc;n H&agrave;n Quốc v&agrave; Việt Nam. Kh&aacute;m ph&aacute; hơn 100 bức tranh tường vẽ đẹp mắt c&ugrave;ng với hướng dẫn vi&ecirc;n n&oacute;i tiếng Anh v&agrave; chắc chắn sẽ c&oacute; được nhiều bức ảnh để bạn nhớ về ng&ocirc;i l&agrave;ng! Trước khi biết rằng bạn sẽ nhớ n&oacute;, bạn sẽ trở lại tr&ecirc;n xe đưa đ&oacute;n v&agrave; trở về kh&aacute;ch sạn của bạn ở Đ&agrave; Nẵng.</p>
</div>

<div><img alt="Tam Thanh Mural Village tour from Da Nang" class="lazy lazy-loaded" src="https://res.klook.com/images/fl_lossy.progressive,q_65/c_fill,w_960,h_640,f_auto/w_59,x_11,y_11,g_south_west,l_klook_water/activities/kozffwd2ldq22oxoh0yu/TourN%E1%BB%ADaNg%C3%A0yL%C3%A0ngB%C3%ADchH%E1%BB%8DaTamThanht%E1%BB%AB%C4%90%C3%A0N%E1%BA%B5ng.webp" style="border-style:none; box-sizing:inherit; display:inline; height:526.667px; margin-bottom:3px; width:790px" />
<div>Trải nghiệm dịch vụ đưa đ&oacute;n thuận tiện từ Đ&agrave; Nẵng bằng xe đưa đ&oacute;n</div>
</div>

<div><img alt="Vietnamese Heroic Mother Statue" class="lazy lazy-loaded" src="https://res.klook.com/images/fl_lossy.progressive,q_65/c_fill,w_1295,h_777,f_auto/w_80,x_15,y_15,g_south_west,l_klook_water/activities/qlzz4fbxrvjywgaplsuf/TourN%E1%BB%ADaNg%C3%A0yL%C3%A0ngB%C3%ADchH%E1%BB%8DaTamThanht%E1%BB%AB%C4%90%C3%A0N%E1%BA%B5ng.webp" style="border-style:none; box-sizing:inherit; display:inline; height:474px; margin-bottom:3px; width:790px" />
<div>Ngắm Tượng Mẹ Việt Nam Anh H&ugrave;ng c&aacute;ch cả nhiều dặm khi bạn đến với Tam Kỳ</div>
</div>

<div><img alt="Tam Thanh Fishing village" class="lazy lazy-loaded" src="https://res.klook.com/images/fl_lossy.progressive,q_65/c_fill,w_1295,h_863,f_auto/w_80,x_15,y_15,g_south_west,l_klook_water/activities/udmhzku5jtiykyzc9xwc/TourN%E1%BB%ADaNg%C3%A0yL%C3%A0ngB%C3%ADchH%E1%BB%8DaTamThanht%E1%BB%AB%C4%90%C3%A0N%E1%BA%B5ng.webp" style="border-style:none; box-sizing:inherit; display:inline; height:526.463px; margin-bottom:3px; width:790px" />
<div>Thăm L&agrave;ng B&iacute;ch Họa Tam Thanh, một l&agrave;ng ch&agrave;i cũ đ&atilde; được biến chuyển bởi nghệ thuật</div>
</div>

<div><img alt="Tam Thanh houses" class="lazy lazy-loaded" src="https://res.klook.com/images/fl_lossy.progressive,q_65/c_fill,w_1295,h_863,f_auto/w_80,x_15,y_15,g_south_west,l_klook_water/activities/rp8j0db6xfuevpou8bhn/TourN%E1%BB%ADaNg%C3%A0yL%C3%A0ngB%C3%ADchH%E1%BB%8DaTamThanht%E1%BB%AB%C4%90%C3%A0N%E1%BA%B5ng.webp" style="border-style:none; box-sizing:inherit; display:inline; height:526.463px; margin-bottom:3px; width:790px" />
<div>Chứng kiến những m&agrave;u sắc sống động v&agrave; những bức tranh tường nghệ thuật v&agrave;o nhịp thở cuộc sống của m&ocirc;i trường xung quanh</div>
</div>
', N'matour2', 1, 1, NULL, 1, 1, 0, CAST(N'2020-03-15T00:00:00.000' AS DateTime), CAST(N'2020-03-15T00:00:00.000' AS DateTime), 15)
INSERT [dbo].[tour] ([id], [ten], [hinhanh], [thoigian], [gia], [tomtat], [chitiet], [matour], [phuongtien_fk], [hienthi], [noibat], [nguoitao_fk], [diadiem_fk], [daxoa], [ngaytao], [ngaycapnhat], [giamgia]) VALUES (1003, N'Tour du lịch Pleiku - Kom Tum - Buôn Mê Thuật 4 Ngày 3 Đêm dành cho 5 người', N'/UserUpload/images/11.jpg', CAST(N'2020-06-15T00:00:00.000' AS DateTime), 6900000, N'Tour du lịch Pleiku - Kom Tum - Buôn Mê Thuật', N'<p>Ng&agrave;y 01<br />
25-06-2020<br />
&nbsp;&nbsp;&nbsp;TP. HỒ CH&Iacute; MINH - PLEIKU Ăn: 2 bữa (trưa, chiều)<br />
Qu&yacute; kh&aacute;ch tập trung tại s&acirc;n bay T&acirc;n Sơn Nhất, Ga đi Trong Nước. Hướng dẫn vi&ecirc;n l&agrave;m thủ tục cho Qu&yacute; kh&aacute;ch đ&aacute;p chuyến bay đi Pleiku. Đến s&acirc;n bay, xe đ&oacute;n đo&agrave;n đi tham quan:<br />
- Quảng trường Đại Đo&agrave;n Kết: được v&iacute; như tr&aacute;i tim của phố n&uacute;i Pleiku, nổi bật với h&igrave;nh ảnh b&aacute;c Hồ cao gần 11m, đứng tr&ecirc;n bệ b&ecirc; t&ocirc;ng ốp đ&aacute; xanh cao 4.5m. Ph&iacute;a sau tượng B&aacute;c l&agrave; d&atilde;y ph&ugrave; đi&ecirc;u m&ocirc; phỏng h&igrave;nh hoa sen được c&aacute;ch điệu bằng đ&aacute; uốn cong, như rừng n&uacute;i T&acirc;y Nguy&ecirc;n bạt ng&agrave;n v&agrave; t&aacute;i hiện lại cuộc sống sinh hoạt, sản xuất của người d&acirc;n Gia Lai.<br />
Sau khi ăn trưa, Qu&yacute; kh&aacute;ch về nhận ph&ograve;ng kh&aacute;ch sạn nghỉ ngơi. Buổi chiều đo&agrave;n tiếp tục tham quan:<br />
- Thủy Điện Yaly: được v&iacute; như l&agrave; một luồn s&aacute;ng lộng lẫy giữa n&uacute;i rừng T&acirc;y Nguy&ecirc;n v&agrave; l&agrave; niềm tự h&agrave;o của đồng b&agrave;o c&aacute;c d&acirc;n tộc nơi đ&acirc;y. Quan trọng hơn cả l&agrave; cảnh sắc tại nh&agrave; m&aacute;y thủy điện v&ocirc; c&ugrave;ng tuyệt vời với những rừng c&acirc;y xanh b&aacute;t ng&aacute;t tr&ecirc;n đồi đất đỏ, với gi&oacute; nh&egrave; nhẹ v&agrave; c&aacute;i nắng v&agrave;ng đầu hạ. Trời th&ecirc;m ch&uacute;t se lạnh khiến du kh&aacute;ch như c&oacute; cảm gi&aacute;c bị lạc v&agrave;o một g&oacute;c nhỏ của Ch&acirc;u &Acirc;u.<br />
- Biển hồ Tơ Nưng: miệng n&uacute;i lửa đ&atilde; ngừng hoạt động h&agrave;ng trăm triệu năm v&agrave; được v&iacute; như &ldquo;Đ&ocirc;i mắt Pleiku&rdquo; l&agrave;m say l&ograve;ng người bởi vẻ đẹp thơ mộng, mặt nước m&ecirc;nh m&ocirc;ng, m&acirc;y trời lồng lộng, nghe tho&aacute;ng trong gi&oacute; tiếng th&ocirc;ng vi vu, tiết trời dịu m&aacute;t quanh năm. Đ&acirc;y c&ograve;n l&agrave; hồ chứa nước ngọt cung cấp nước cho th&agrave;nh phố Pleiku. Qu&yacute; kh&aacute;ch chi&ecirc;m b&aacute;i Bảo Tượng Quan &Acirc;m cao 15 m&eacute;t được l&agrave;m bằng chất liệu đ&aacute; cẩm thạch trắng, điểm nhấn mới vừa đưa v&agrave;o khai th&aacute;c từ những th&aacute;ng cuối năm 2018.&nbsp;<br />
Ăn chiều, Buổi tối, Qu&yacute; kh&aacute;ch tự do dạo phố đ&ecirc;m.&nbsp;<br />
Nghỉ đ&ecirc;m tại Pleiku. Buổi tối, Qu&yacute; kh&aacute;ch tự do dạo phố đ&ecirc;m.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 02<br />
26-06-2020<br />
&nbsp;&nbsp;&nbsp;PLEIKU &ndash; KON TUM &ndash; KDL MĂNG ĐEN &ndash; NH&Agrave; R&Ocirc;NG KON KLOR Ăn: 3 bữa (s&aacute;ng, trưa, chiều)<br />
Sau bữa s&aacute;ng, xe đưa Q&uacute;y kh&aacute;ch khởi h&agrave;nh đi Kon Tum. Tr&ecirc;n đường đi Qu&yacute; kh&aacute;ch sẽ được chi&ecirc;m ngưỡng Cầu treo Kon Klor - l&agrave; chiếc cầu d&acirc;y văng to đẹp nhất khu vực T&acirc;y Nguy&ecirc;n, nối liền hai bờ của d&ograve;ng s&ocirc;ng Đăk Bla. Cầu treo Kor Klor g&acirc;y ấn tượng với du kh&aacute;ch với m&agrave;u cam nổi bật, được x&acirc;y dựng ho&agrave;n to&agrave;n bằng sắt th&eacute;p ki&ecirc;n cố bao quanh giữa giữa v&ugrave;ng rừng n&uacute;i T&acirc;y Nguy&ecirc;n h&ugrave;ng vĩ phủ nương d&acirc;u xanh r&igrave;. Đến Kon Tum đo&agrave;n tham quan:<br />
- Nh&agrave; r&ocirc;ng Kon Klor &ndash; l&agrave; nh&agrave; r&ocirc;ng to v&agrave; đẹp nhất, đ&atilde; trở th&agrave;nh niềm ki&ecirc;u h&atilde;nh của người T&acirc;y Nguy&ecirc;n.<br />
- Nh&agrave; Thờ Ch&iacute;nh T&ograve;a &ndash; ng&ocirc;i nh&agrave; thờ gỗ hơn 100 tuổi, l&agrave; một di t&iacute;ch cổ v&agrave; đẹp nhất ở Kon Tum. Nơi đ&acirc;y lu&ocirc;n l&agrave; điểm đến được đ&aacute;nh dấu trong bản đồ du lịch của nhiều du kh&aacute;ch khi tới th&agrave;nh phố cao nguy&ecirc;n trẻ trung v&agrave; đầy năng động n&agrave;y.<br />
- Khu Du Lịch Măng Đen &ndash; được v&iacute; như Đ&agrave; Lạt thứ 2 với nhiều hồ th&aacute;c, suối đ&aacute; v&agrave; cảnh quan thi&ecirc;n nhi&ecirc;n, văn h&oacute;a bản địa đặc sắc. Đến với Măng Đen bạn sẽ được thưởng thức những m&oacute;n đặc sản c&oacute; một kh&ocirc;ng hai của n&uacute;i rừng nơi đ&acirc;y như: G&agrave; tầm Măng đen, heo quay Măng đen, rượu cần, cơm lam, g&agrave; nướng măng đen (chi ph&iacute; tự t&uacute;c) &hellip;<br />
Đo&agrave;n trở về Pleiku, ăn tối. Nghỉ đ&ecirc;m tại Pleiku.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 03<br />
27-06-2020<br />
&nbsp;&nbsp;&nbsp;PLEIKU &ndash; BU&Ocirc;N MA THUỘT &ndash; BẢO T&Agrave;NG THẾ GIỚI C&Agrave; PH&Ecirc; &ndash; KDL SUỐI ONG Ăn: 3 bữa (s&aacute;ng, trưa, chiều)<br />
Sau bữa s&aacute;ng, Qu&yacute; kh&aacute;ch trả ph&ograve;ng kh&aacute;ch sạn khởi h&agrave;nh đi Bu&ocirc;n Ma Thuột. Ăn trưa. Đo&agrave;n tiếp tục tham quan<br />
- Bảo t&agrave;ng thế giới C&agrave; Ph&ecirc; - Đ&acirc;y l&agrave; một tổ hợp bao gồm c&aacute;c kh&ocirc;ng gian trưng b&agrave;y bảo t&agrave;ng, kh&ocirc;ng gian triển l&atilde;m, kh&ocirc;ng gian thư viện &aacute;nh s&aacute;ng, kh&ocirc;ng gian thưởng l&atilde;m c&agrave; ph&ecirc;, kh&ocirc;ng gian hội thảo&hellip; c&aacute;c kh&ocirc;ng gian n&agrave;y được kết nối với c&aacute;c kh&ocirc;ng gian mang t&iacute;nh mở trong c&ocirc;ng vi&ecirc;n c&agrave; ph&ecirc;. Bảo t&agrave;ng thế giới c&agrave; ph&ecirc; đ&atilde; th&agrave;nh h&igrave;nh với kiến tr&uacute;c nương theo kh&ocirc;ng gian nh&agrave; d&agrave;i quen thuộc đặc trưng của v&ugrave;ng đất T&acirc;y Nguy&ecirc;n linh thi&ecirc;ng. Những đường cong đa h&igrave;nh v&agrave; uyển chuyển được giao thoa với nhau tạo&nbsp;<br />
Buổi chiều, xe đưa đo&agrave;n đi tham quan:&nbsp;<br />
- KDL Suối Ong: kiến tr&uacute;c được lấy cảm hứng từ &quot;lo&agrave;i ong&quot; n&ecirc;n c&aacute;c thiết kế từ c&aacute;c &ocirc; gạch đến khu trưng b&agrave;y mật ong đều được tạo dựng từ h&igrave;nh lục gi&aacute;c của tổ ong. Du kh&aacute;ch thỏa th&iacute;ch s&aacute;ng tạo với những tiểu cảnh như Hồ C&aacute; Koi, Khu vườn M&ecirc; Cung, N&uacute;i Đ&aacute; Nh&acirc;n Tạo,... Ngo&agrave;i ra, Qu&yacute; kh&aacute;ch c&ograve;n c&oacute; thể t&igrave;m hiểu quy tr&igrave;nh lấy mật ong tại đ&acirc;y.<br />
Ăn chiều, nhận ph&ograve;ng kh&aacute;ch sạn. Buổi tối, Qu&yacute; kh&aacute;ch tự do h&ograve;a m&igrave;nh v&agrave;o nhịp sống của người d&acirc;n phố n&uacute;i về đ&ecirc;m như thưởng thức &ldquo;ly c&agrave; ph&ecirc; Ban M&ecirc;&rdquo; trứ danh ngắm phố l&ecirc;n đ&egrave;n hay gh&eacute; chợ đ&ecirc;m Ng&atilde; S&aacute;u thưởng thức ẩm thực đường phố như b&uacute;n đỏ, b&uacute;n ri&ecirc;u, b&uacute;n gi&ograve;, canh c&aacute; dằm&hellip; hoặc đơn giản l&agrave; ngồi nh&acirc;m nhi ly sữa n&oacute;ng trong tiết trời se lạnh.<br />
Nghỉ đ&ecirc;m tại Bu&ocirc;n Ma Thuột.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 04<br />
28-06-2020<br />
&nbsp;&nbsp;&nbsp;BU&Ocirc;N MA THUỘT - TP. HỒ CH&Iacute; MINH Ăn: 2 bữa (Ăn s&aacute;ng, trưa)<br />
Sau bữa s&aacute;ng, Q&uacute;y kh&aacute;ch trả ph&ograve;ng kh&aacute;ch sạn. Đo&agrave;n tiếp tục tham quan:<br />
- KDL Bu&ocirc;n Đ&ocirc;n - Qu&yacute; kh&aacute;ch sẽ c&oacute; cơ hội trải nghiệm một cảm gi&aacute;c tuyệt vời khi được tự m&igrave;nh cưỡi tr&ecirc;n lưng của những ch&uacute; Voi hiền l&agrave;nh tại đ&acirc;y (chi ph&iacute; tự t&uacute;c) v&agrave; c&ugrave;ng ngắm cảnh h&ugrave;ng vĩ của Rừng Nguy&ecirc;n Sinh Quốc Gia Yok Đ&ocirc;n, tham quan cầu treo v&agrave; ngắm cảnh s&ocirc;ng Serep&ocirc;k, nh&agrave; s&agrave;n cổ 120 năm của người L&agrave;o, t&igrave;m hiểu truyền thuyết Vua Săn Voi, thăm bu&ocirc;n l&agrave;ng của người d&acirc;n tộc: L&agrave;o, Khơme, &Ecirc;đ&ecirc;, M&rsquo;N&ocirc;ng....&nbsp;<br />
Ăn trưa, đo&agrave;n khởi h&agrave;nh tham quan:<br />
- Vườn Caf&eacute; R&ecirc;u Phong: được dịp tận mắt xem vườn ca cao, loại tr&aacute;i c&acirc;y được trồng chủ yếu để lấy hạt l&agrave;m n&ecirc;n c&aacute;c sản phẩm kinh tế cao tr&ecirc;n thị trường hiện nay đ&oacute; l&agrave; bột ca cao v&agrave; chocolate. Ngo&agrave;i ra, Qu&yacute; kh&aacute;ch sẽ được thưởng thức đặc sản của qu&aacute;n &ldquo;sinh tố ca cao&rdquo; được l&agrave;m từ cơm của tr&aacute;i ca cao, vị chua chua ngọt ngọt, một thức uống giải nhiệt lạ miệng nhưng cũng kh&ocirc;ng k&eacute;m phần hấp dẫn.<br />
Sau bữa chiều, xe đưa đo&agrave;n ra s&acirc;n bay Bu&ocirc;n Ma Thuột đ&aacute;p chuyến bay về TP.HCM. Chia tay Qu&yacute; kh&aacute;ch v&agrave; kết th&uacute;c chương tr&igrave;nh tại s&acirc;n bay T&acirc;n Sơn Nhất.<br />
&nbsp;</p>

<div><img src="chrome-extension://bpggmmljdiliancllaapiggllnkbjocb/icon/16.png" /></div>
', N'01', 1, 1, 1, 1, 2, 0, CAST(N'2020-03-15T00:00:00.000' AS DateTime), CAST(N'2020-03-15T00:00:00.000' AS DateTime), 10)
INSERT [dbo].[tour] ([id], [ten], [hinhanh], [thoigian], [gia], [tomtat], [chitiet], [matour], [phuongtien_fk], [hienthi], [noibat], [nguoitao_fk], [diadiem_fk], [daxoa], [ngaytao], [ngaycapnhat], [giamgia]) VALUES (1004, N'Pleiku - Kom Tum - Buôn Ma Thuật 3 Ngày 2 Đêm dành cho 3 người', N'/UserUpload/images/hinh2.jpg', CAST(N'2020-06-15T00:00:00.000' AS DateTime), 5900000, N'Tour du lịch Pleiku - Kom Tum - Buôn Mê Thuật', N'<p>Ng&agrave;y 01<br />
25-06-2020<br />
&nbsp;&nbsp;&nbsp;TP. HỒ CH&Iacute; MINH - PLEIKU Ăn: 2 bữa (trưa, chiều)<br />
Qu&yacute; kh&aacute;ch tập trung tại s&acirc;n bay T&acirc;n Sơn Nhất, Ga đi Trong Nước. Hướng dẫn vi&ecirc;n l&agrave;m thủ tục cho Qu&yacute; kh&aacute;ch đ&aacute;p chuyến bay đi Pleiku. Đến s&acirc;n bay, xe đ&oacute;n đo&agrave;n đi tham quan:<br />
- Quảng trường Đại Đo&agrave;n Kết: được v&iacute; như tr&aacute;i tim của phố n&uacute;i Pleiku, nổi bật với h&igrave;nh ảnh b&aacute;c Hồ cao gần 11m, đứng tr&ecirc;n bệ b&ecirc; t&ocirc;ng ốp đ&aacute; xanh cao 4.5m. Ph&iacute;a sau tượng B&aacute;c l&agrave; d&atilde;y ph&ugrave; đi&ecirc;u m&ocirc; phỏng h&igrave;nh hoa sen được c&aacute;ch điệu bằng đ&aacute; uốn cong, như rừng n&uacute;i T&acirc;y Nguy&ecirc;n bạt ng&agrave;n v&agrave; t&aacute;i hiện lại cuộc sống sinh hoạt, sản xuất của người d&acirc;n Gia Lai.<br />
Sau khi ăn trưa, Qu&yacute; kh&aacute;ch về nhận ph&ograve;ng kh&aacute;ch sạn nghỉ ngơi. Buổi chiều đo&agrave;n tiếp tục tham quan:<br />
- Thủy Điện Yaly: được v&iacute; như l&agrave; một luồn s&aacute;ng lộng lẫy giữa n&uacute;i rừng T&acirc;y Nguy&ecirc;n v&agrave; l&agrave; niềm tự h&agrave;o của đồng b&agrave;o c&aacute;c d&acirc;n tộc nơi đ&acirc;y. Quan trọng hơn cả l&agrave; cảnh sắc tại nh&agrave; m&aacute;y thủy điện v&ocirc; c&ugrave;ng tuyệt vời với những rừng c&acirc;y xanh b&aacute;t ng&aacute;t tr&ecirc;n đồi đất đỏ, với gi&oacute; nh&egrave; nhẹ v&agrave; c&aacute;i nắng v&agrave;ng đầu hạ. Trời th&ecirc;m ch&uacute;t se lạnh khiến du kh&aacute;ch như c&oacute; cảm gi&aacute;c bị lạc v&agrave;o một g&oacute;c nhỏ của Ch&acirc;u &Acirc;u.<br />
- Biển hồ Tơ Nưng: miệng n&uacute;i lửa đ&atilde; ngừng hoạt động h&agrave;ng trăm triệu năm v&agrave; được v&iacute; như &ldquo;Đ&ocirc;i mắt Pleiku&rdquo; l&agrave;m say l&ograve;ng người bởi vẻ đẹp thơ mộng, mặt nước m&ecirc;nh m&ocirc;ng, m&acirc;y trời lồng lộng, nghe tho&aacute;ng trong gi&oacute; tiếng th&ocirc;ng vi vu, tiết trời dịu m&aacute;t quanh năm. Đ&acirc;y c&ograve;n l&agrave; hồ chứa nước ngọt cung cấp nước cho th&agrave;nh phố Pleiku. Qu&yacute; kh&aacute;ch chi&ecirc;m b&aacute;i Bảo Tượng Quan &Acirc;m cao 15 m&eacute;t được l&agrave;m bằng chất liệu đ&aacute; cẩm thạch trắng, điểm nhấn mới vừa đưa v&agrave;o khai th&aacute;c từ những th&aacute;ng cuối năm 2018.&nbsp;<br />
Ăn chiều, Buổi tối, Qu&yacute; kh&aacute;ch tự do dạo phố đ&ecirc;m.&nbsp;<br />
Nghỉ đ&ecirc;m tại Pleiku. Buổi tối, Qu&yacute; kh&aacute;ch tự do dạo phố đ&ecirc;m.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 02<br />
26-06-2020<br />
&nbsp;&nbsp;&nbsp;PLEIKU &ndash; KON TUM &ndash; KDL MĂNG ĐEN &ndash; NH&Agrave; R&Ocirc;NG KON KLOR Ăn: 3 bữa (s&aacute;ng, trưa, chiều)<br />
Sau bữa s&aacute;ng, xe đưa Q&uacute;y kh&aacute;ch khởi h&agrave;nh đi Kon Tum. Tr&ecirc;n đường đi Qu&yacute; kh&aacute;ch sẽ được chi&ecirc;m ngưỡng Cầu treo Kon Klor - l&agrave; chiếc cầu d&acirc;y văng to đẹp nhất khu vực T&acirc;y Nguy&ecirc;n, nối liền hai bờ của d&ograve;ng s&ocirc;ng Đăk Bla. Cầu treo Kor Klor g&acirc;y ấn tượng với du kh&aacute;ch với m&agrave;u cam nổi bật, được x&acirc;y dựng ho&agrave;n to&agrave;n bằng sắt th&eacute;p ki&ecirc;n cố bao quanh giữa giữa v&ugrave;ng rừng n&uacute;i T&acirc;y Nguy&ecirc;n h&ugrave;ng vĩ phủ nương d&acirc;u xanh r&igrave;. Đến Kon Tum đo&agrave;n tham quan:<br />
- Nh&agrave; r&ocirc;ng Kon Klor &ndash; l&agrave; nh&agrave; r&ocirc;ng to v&agrave; đẹp nhất, đ&atilde; trở th&agrave;nh niềm ki&ecirc;u h&atilde;nh của người T&acirc;y Nguy&ecirc;n.<br />
- Nh&agrave; Thờ Ch&iacute;nh T&ograve;a &ndash; ng&ocirc;i nh&agrave; thờ gỗ hơn 100 tuổi, l&agrave; một di t&iacute;ch cổ v&agrave; đẹp nhất ở Kon Tum. Nơi đ&acirc;y lu&ocirc;n l&agrave; điểm đến được đ&aacute;nh dấu trong bản đồ du lịch của nhiều du kh&aacute;ch khi tới th&agrave;nh phố cao nguy&ecirc;n trẻ trung v&agrave; đầy năng động n&agrave;y.<br />
- Khu Du Lịch Măng Đen &ndash; được v&iacute; như Đ&agrave; Lạt thứ 2 với nhiều hồ th&aacute;c, suối đ&aacute; v&agrave; cảnh quan thi&ecirc;n nhi&ecirc;n, văn h&oacute;a bản địa đặc sắc. Đến với Măng Đen bạn sẽ được thưởng thức những m&oacute;n đặc sản c&oacute; một kh&ocirc;ng hai của n&uacute;i rừng nơi đ&acirc;y như: G&agrave; tầm Măng đen, heo quay Măng đen, rượu cần, cơm lam, g&agrave; nướng măng đen (chi ph&iacute; tự t&uacute;c) &hellip;<br />
Đo&agrave;n trở về Pleiku, ăn tối. Nghỉ đ&ecirc;m tại Pleiku.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 03<br />
27-06-2020<br />
&nbsp;&nbsp;&nbsp;PLEIKU &ndash; BU&Ocirc;N MA THUỘT &ndash; BẢO T&Agrave;NG THẾ GIỚI C&Agrave; PH&Ecirc; &ndash; KDL SUỐI ONG Ăn: 3 bữa (s&aacute;ng, trưa, chiều)<br />
Sau bữa s&aacute;ng, Qu&yacute; kh&aacute;ch trả ph&ograve;ng kh&aacute;ch sạn khởi h&agrave;nh đi Bu&ocirc;n Ma Thuột. Ăn trưa. Đo&agrave;n tiếp tục tham quan<br />
- Bảo t&agrave;ng thế giới C&agrave; Ph&ecirc; - Đ&acirc;y l&agrave; một tổ hợp bao gồm c&aacute;c kh&ocirc;ng gian trưng b&agrave;y bảo t&agrave;ng, kh&ocirc;ng gian triển l&atilde;m, kh&ocirc;ng gian thư viện &aacute;nh s&aacute;ng, kh&ocirc;ng gian thưởng l&atilde;m c&agrave; ph&ecirc;, kh&ocirc;ng gian hội thảo&hellip; c&aacute;c kh&ocirc;ng gian n&agrave;y được kết nối với c&aacute;c kh&ocirc;ng gian mang t&iacute;nh mở trong c&ocirc;ng vi&ecirc;n c&agrave; ph&ecirc;. Bảo t&agrave;ng thế giới c&agrave; ph&ecirc; đ&atilde; th&agrave;nh h&igrave;nh với kiến tr&uacute;c nương theo kh&ocirc;ng gian nh&agrave; d&agrave;i quen thuộc đặc trưng của v&ugrave;ng đất T&acirc;y Nguy&ecirc;n linh thi&ecirc;ng. Những đường cong đa h&igrave;nh v&agrave; uyển chuyển được giao thoa với nhau tạo&nbsp;<br />
Buổi chiều, xe đưa đo&agrave;n đi tham quan:&nbsp;<br />
- KDL Suối Ong: kiến tr&uacute;c được lấy cảm hứng từ &quot;lo&agrave;i ong&quot; n&ecirc;n c&aacute;c thiết kế từ c&aacute;c &ocirc; gạch đến khu trưng b&agrave;y mật ong đều được tạo dựng từ h&igrave;nh lục gi&aacute;c của tổ ong. Du kh&aacute;ch thỏa th&iacute;ch s&aacute;ng tạo với những tiểu cảnh như Hồ C&aacute; Koi, Khu vườn M&ecirc; Cung, N&uacute;i Đ&aacute; Nh&acirc;n Tạo,... Ngo&agrave;i ra, Qu&yacute; kh&aacute;ch c&ograve;n c&oacute; thể t&igrave;m hiểu quy tr&igrave;nh lấy mật ong tại đ&acirc;y.<br />
Ăn chiều, nhận ph&ograve;ng kh&aacute;ch sạn. Buổi tối, Qu&yacute; kh&aacute;ch tự do h&ograve;a m&igrave;nh v&agrave;o nhịp sống của người d&acirc;n phố n&uacute;i về đ&ecirc;m như thưởng thức &ldquo;ly c&agrave; ph&ecirc; Ban M&ecirc;&rdquo; trứ danh ngắm phố l&ecirc;n đ&egrave;n hay gh&eacute; chợ đ&ecirc;m Ng&atilde; S&aacute;u thưởng thức ẩm thực đường phố như b&uacute;n đỏ, b&uacute;n ri&ecirc;u, b&uacute;n gi&ograve;, canh c&aacute; dằm&hellip; hoặc đơn giản l&agrave; ngồi nh&acirc;m nhi ly sữa n&oacute;ng trong tiết trời se lạnh.<br />
Nghỉ đ&ecirc;m tại Bu&ocirc;n Ma Thuột.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 04<br />
28-06-2020<br />
&nbsp;&nbsp;&nbsp;BU&Ocirc;N MA THUỘT - TP. HỒ CH&Iacute; MINH Ăn: 2 bữa (Ăn s&aacute;ng, trưa)<br />
Sau bữa s&aacute;ng, Q&uacute;y kh&aacute;ch trả ph&ograve;ng kh&aacute;ch sạn. Đo&agrave;n tiếp tục tham quan:<br />
- KDL Bu&ocirc;n Đ&ocirc;n - Qu&yacute; kh&aacute;ch sẽ c&oacute; cơ hội trải nghiệm một cảm gi&aacute;c tuyệt vời khi được tự m&igrave;nh cưỡi tr&ecirc;n lưng của những ch&uacute; Voi hiền l&agrave;nh tại đ&acirc;y (chi ph&iacute; tự t&uacute;c) v&agrave; c&ugrave;ng ngắm cảnh h&ugrave;ng vĩ của Rừng Nguy&ecirc;n Sinh Quốc Gia Yok Đ&ocirc;n, tham quan cầu treo v&agrave; ngắm cảnh s&ocirc;ng Serep&ocirc;k, nh&agrave; s&agrave;n cổ 120 năm của người L&agrave;o, t&igrave;m hiểu truyền thuyết Vua Săn Voi, thăm bu&ocirc;n l&agrave;ng của người d&acirc;n tộc: L&agrave;o, Khơme, &Ecirc;đ&ecirc;, M&rsquo;N&ocirc;ng....&nbsp;<br />
Ăn trưa, đo&agrave;n khởi h&agrave;nh tham quan:<br />
- Vườn Caf&eacute; R&ecirc;u Phong: được dịp tận mắt xem vườn ca cao, loại tr&aacute;i c&acirc;y được trồng chủ yếu để lấy hạt l&agrave;m n&ecirc;n c&aacute;c sản phẩm kinh tế cao tr&ecirc;n thị trường hiện nay đ&oacute; l&agrave; bột ca cao v&agrave; chocolate. Ngo&agrave;i ra, Qu&yacute; kh&aacute;ch sẽ được thưởng thức đặc sản của qu&aacute;n &ldquo;sinh tố ca cao&rdquo; được l&agrave;m từ cơm của tr&aacute;i ca cao, vị chua chua ngọt ngọt, một thức uống giải nhiệt lạ miệng nhưng cũng kh&ocirc;ng k&eacute;m phần hấp dẫn.<br />
Sau bữa chiều, xe đưa đo&agrave;n ra s&acirc;n bay Bu&ocirc;n Ma Thuột đ&aacute;p chuyến bay về TP.HCM. Chia tay Qu&yacute; kh&aacute;ch v&agrave; kết th&uacute;c chương tr&igrave;nh tại s&acirc;n bay T&acirc;n Sơn Nhất.<br />
&nbsp;</p>
', N'02', 1, 1, 1, 1, 2, 0, CAST(N'2020-06-15T00:00:00.000' AS DateTime), CAST(N'2020-06-15T00:00:00.000' AS DateTime), 15)
INSERT [dbo].[tour] ([id], [ten], [hinhanh], [thoigian], [gia], [tomtat], [chitiet], [matour], [phuongtien_fk], [hienthi], [noibat], [nguoitao_fk], [diadiem_fk], [daxoa], [ngaytao], [ngaycapnhat], [giamgia]) VALUES (1005, N'Tour du lịch Quy Nhơn - Kì Co 2 Ngày 1 Đêm dành cho 2 người', N'/UserUpload/images/33.png', CAST(N'2020-06-15T00:00:00.000' AS DateTime), 5000000, N'Tour du lịch Quy Nhơn - Kì Co 2 Ngày 1 Đêm', N'<p>TP.HỒ CH&Iacute; MINH - QUY NHƠN Số bữa ăn: 2 (Ăn trưa, chiều)<br />
Qu&yacute; kh&aacute;ch tập trung tại cột 5, ga đi Trong Nước, s&acirc;n bay T&acirc;n Sơn Nhất. Hướng dẫn vi&ecirc;n Vietravel hỗ trợ l&agrave;m thủ tục cho đo&agrave;n đ&aacute;p chuyến bay đi Quy Nhơn. Tại s&acirc;n bay Ph&ugrave; C&aacute;t &ndash; Quy Nhơn, xe v&agrave; hướng dẫn vi&ecirc;n sẽ đ&oacute;n Qu&yacute; kh&aacute;ch đi tham quan:&nbsp;<br />
- Bảo T&agrave;ng Quang Trung: viếng Điện thờ T&acirc;y Sơn Tam Kiệt, chi&ecirc;m ngưỡng c&acirc;y me 300 tuổi, Giếng nước xưa&hellip;&nbsp;<br />
- Khu du lịch Hầm H&ocirc;: L&acirc;m vi&ecirc;n hoang d&atilde; v&agrave; n&ecirc;n thơ, đo&agrave;n kh&aacute;m ph&aacute; non nước hữu t&igrave;nh. B&ecirc;n cạnh đ&oacute;, Qu&yacute; kh&aacute;ch c&oacute; thể lựa chọn th&ecirc;m dịch vụ kh&aacute;c như: xe ngựa, c&acirc;u c&aacute; thư gi&atilde;n, ch&egrave;o thuyền tr&ecirc;n s&ocirc;ng K&uacute;t, nghỉ v&otilde;ng dưới t&aacute;n c&acirc;y rừng đại ng&agrave;n&hellip;(chi ph&iacute; tự t&uacute;c)&nbsp;<br />
- Nhận ph&ograve;ng. Qu&yacute; kh&aacute;ch tự do nghỉ ngơi, tắm biển hoặc hồ bơi tại Resort FLC Quy Nhơn 5 sao.<br />
- Buổi tối Qu&yacute; kh&aacute;ch tự do kh&aacute;m ph&aacute; Quy Nhơn về đ&ecirc;m với c&aacute;c qu&aacute;n c&agrave; ph&ecirc; nổi tiếng check in: Surf Bar, S- Blue Caf&eacute;, Mộc Tr&agrave; Caf&eacute;&hellip;hoặc thưởng thức hải sản tại phố ẩm thực Xu&acirc;n Diệu, khu hồ sinh th&aacute;i nổi tiếng với m&oacute;n: bọ biển, cua Huỳnh Đế v&agrave; c&aacute;c loại ốc đặc trưng của v&ugrave;ng (chi ph&iacute; tự t&uacute;c). Nghỉ đ&ecirc;m tại Quy Nhơn.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 02<br />
02-07-2020<br />
&nbsp;&nbsp;&nbsp;QUY NHƠN - EO GI&Oacute; - KỲ CO Số bữa ăn: 3 (Ăn s&aacute;ng, trưa, chiều)<br />
Ăn s&aacute;ng buffet tại Resort. Qu&yacute; kh&aacute;ch tự do nghỉ ngơi tắm biển hoặc hồ bơi tại Resort hoặc c&ugrave;ng đo&agrave;n đi tham quan:&nbsp;<br />
- Eo Gi&oacute;: nơi vẫn c&ograve;n giữ được cho m&igrave;nh vẻ đẹp tự nhi&ecirc;n đầy hoang sơ. Đứng từ tr&ecirc;n cao nh&igrave;n xuống, Eo Gi&oacute; như một bức tranh sơn thủy hữu t&igrave;nh với trời, m&acirc;y, nước&hellip; biển n&uacute;i &ocirc;m nhau vừa gợi cảm, vừa hoang sơ đầy quyến rũ.<br />
- Kỳ Co: với m&agrave;u nước trong xanh, b&atilde;i c&aacute;t trắng d&agrave;i v&agrave; mịn, những cơn s&oacute;ng biển &ecirc;m &aacute;i ch&iacute;nh l&agrave; điểm đến l&yacute; tưởng thu h&uacute;t nhiều du kh&aacute;ch trong thời gian gần đ&acirc;y. Ngo&agrave;i tắm biển v&agrave; đ&ugrave;a vui, qu&yacute; kh&aacute;ch c&ograve;n c&oacute; cơ hội đến B&atilde;i San H&ocirc; ngắm những rặng san h&ocirc; tự nhi&ecirc;n v&agrave; h&agrave;ng trăm lo&agrave;i c&aacute; đầy m&agrave;u sắc tung tăng bơi lội.&nbsp;<br />
- Khu du lịch Ghềnh R&aacute;ng Ti&ecirc;n Sa, viếng Mộ H&agrave;n Mặc Tử: mộ nằm dựa lưng v&agrave;o n&uacute;i, nh&igrave;n bao qu&aacute;t cả dải bờ biển Quy Nhơn chạy d&agrave;i trước mặt, h&uacute;t trọn tầm mắt một phần th&agrave;nh phố Quy Nhơn.<br />
- Mua sắm c&aacute;c đặc sản Quy Nhơn tại shop Phương Nghi như Rượu B&agrave;u Đ&aacute;, Nem Chợ Huyện, B&aacute;nh Hồng, Tr&eacute; B&igrave;nh Định, B&aacute;nh &Iacute;t L&aacute; Gai&hellip;<br />
Buổi tối Qu&yacute; kh&aacute;ch tự do nghỉ ngơi tại Resort FLC Quy Nhơn 5 sao. Nghỉ đ&ecirc;m tại Quy Nhơn.&nbsp;<br />
&nbsp;<br />
Ghi ch&uacute;: Trong trường hợp Qu&yacute; kh&aacute;ch kh&ocirc;ng đi tham quan Kỳ Co, ở lại resort tự do tắm biển hoặc nghỉ ngơi&hellip; vui l&ograve;ng b&aacute;o ngay khi đăng k&yacute; hoặc thanh to&aacute;n, Vietravel sẽ ho&agrave;n chi ph&iacute;: ăn trưa v&agrave; ph&iacute; tham quan l&agrave; 300.000VND/ kh&aacute;ch. Trong trường hợp b&aacute;o sau sẽ kh&ocirc;ng ho&agrave;n giảm.&nbsp;<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 03<br />
03-07-2020<br />
&nbsp;&nbsp;&nbsp;QUY NHƠN &ndash; TP.HỒ CH&Iacute; MINH Số bữa ăn: 1 (Ăn s&aacute;ng)<br />
Buối s&aacute;ng Qu&yacute; kh&aacute;ch tự do tắm biển hoặc hồ bơi tại Resort đến giờ trả ph&ograve;ng. Xe đưa đo&agrave;n đi tham quan:&nbsp;<br />
- Ch&ugrave;a Thi&ecirc;n Hưng: nơi hiện đang lưu giữ Ngọc X&aacute; Lợi của Phật Tổ Th&iacute;ch Ca M&acirc;u Ni, một trong những điểm tham quan kh&ocirc;ng thể thiếu khi đến Quy Nhơn - B&igrave;nh Định.&nbsp;&nbsp;<br />
Sau đ&oacute;, xe đưa đo&agrave;n ra s&acirc;n bay Ph&ugrave; C&aacute;t - Quy Nhơn đ&aacute;p chuyến bay về TP.Hồ Ch&iacute; Minh. Chia tay Qu&yacute; kh&aacute;ch v&agrave; kết th&uacute;c chương tr&igrave;nh tại s&acirc;n bay T&acirc;n Sơn Nhất.<br />
&nbsp;<br />
Note: Trong trường hợp chuyến bay về sau 14:00 hỗ trợ tặng ăn trưa.<br />
&nbsp;</p>

<div><img src="chrome-extension://bpggmmljdiliancllaapiggllnkbjocb/icon/16.png" /></div>
', N'03', 1, 1, 1, 1, 3, 0, CAST(N'2020-06-15T00:00:00.000' AS DateTime), CAST(N'2020-06-15T00:00:00.000' AS DateTime), 20)
INSERT [dbo].[tour] ([id], [ten], [hinhanh], [thoigian], [gia], [tomtat], [chitiet], [matour], [phuongtien_fk], [hienthi], [noibat], [nguoitao_fk], [diadiem_fk], [daxoa], [ngaytao], [ngaycapnhat], [giamgia]) VALUES (1006, N'Đà Lạt Tour 1 Ngày 1 Đêm dành cho 2 người', N'/UserUpload/images/55.jpg', CAST(N'2020-06-15T00:00:00.000' AS DateTime), 1600000, N'Đà Lạt Tour 1 Ngày 1 Đêm', N'<p>TP.HCM - Đ&Agrave; LẠT &ndash; QU&Ecirc; GARDEN (Ăn s&aacute;ng, trưa, tối)<br />
Qu&yacute; kh&aacute;ch tập trung tại điểm hẹn, xe đưa đo&agrave;n khởi h&agrave;nh đi Đ&agrave; Lạt. Đo&agrave;n dừng cơm trưa tại Bảo Lộc, tiếp tục h&agrave;nh tr&igrave;nh đến với th&agrave;nh phố ng&agrave;n hoa. Đến Đ&agrave; Lạt, Qu&yacute; kh&aacute;ch sẽ dừng ch&acirc;n tham quan:<br />
Qu&ecirc; Garden: nằm dưới ch&acirc;n đ&egrave;o Mimosa, khu vườn m&ecirc; mẩn người xem với c&aacute;nh đồng hoa rộng lớn c&ugrave;ng nhiều lo&agrave;i hoa nổi tiếng tại Đ&agrave; Lạt khoe sắc. Đặc biệt Qu&ecirc; Garden tự h&agrave;o l&agrave; khu vườn bonsai l&aacute; kim lớn nhất Việt Nam, những chậu c&acirc;y bonsai vừa to vừa đẹp, được chăm s&oacute;c v&agrave; uốn nắn kỹ lưỡng v&ocirc; c&ugrave;ng đẹp mắt. Ngo&agrave;i ra, hồ c&aacute; koi với view nh&igrave;n ra đồi th&ocirc;ng l&agrave; điểm check in cực chất m&agrave; du kh&aacute;ch kh&ocirc;ng thể bỏ qua.<br />
Về kh&aacute;ch sạn, nhận ph&ograve;ng nghỉ ngơi v&agrave; ăn tối. Buổi tối, Qu&yacute; kh&aacute;ch tự do dạo chợ đ&ecirc;m Đ&agrave; Lạt, sắm cho m&igrave;nh chiếc khăn hoặc &aacute;o len để ghi lại những bức ảnh rực rỡ tại phố n&uacute;i xinh đẹp. Nghỉ đ&ecirc;m tại Đ&agrave; Lạt.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 02<br />
20-06-2020<br />
&nbsp;&nbsp;&nbsp;Đ&Agrave; LẠT &ndash; PH&Acirc;N VIỆN SINH HỌC &ndash; GALLERY CHOCOLATE - PUPPY FARM &ndash; ĐỒI CH&Egrave; CẦU ĐẤT - C&Aacute;NH ĐỒNG HOA CẨM T&Uacute; CẦU (Ăn s&aacute;ng, trưa)<br />
Sau khi d&ugrave;ng bữa s&aacute;ng tại kh&aacute;ch sạn, Qu&yacute; kh&aacute;ch khởi h&agrave;nh tham quan v&ugrave;ng đất của những đổi thay với những điểm đến mới:<br />
Ph&acirc;n Viện Sinh Học Đ&agrave; Lạt: được biết đến như 1 tọa độ sống ảo mới tại Đ&agrave; Lạt với g&oacute;c ảnh b&ecirc;n những bức tường cổ k&iacute;nh. Kh&ocirc;ng chỉ vậy, đến đ&acirc;y, du kh&aacute;ch c&ograve;n được t&igrave;m hiểu về m&ocirc;i trường sống, những nguồn gen qu&yacute; qua hiện vật v&ocirc; c&ugrave;ng phong ph&uacute;, khu trưng b&agrave;y xương, sừng, m&ocirc; h&igrave;nh của c&aacute;c lo&agrave;i động vật qu&yacute; hiếm như khủng long, voi, hổ, gấu, vượn,&hellip; hay t&igrave;m hiểu về sự vận h&agrave;nh của vũ trụ.<br />
Gallery Chocolate: với các kh&ocirc;ng gian được tạo n&ecirc;n bởi màu sắc, tạo hình và hương vị đặc bi&ecirc;̣t của nhi&ecirc;̀u sản ph&acirc;̉m của Chocotea - Đà Lạt. &Acirc;́n tượng nh&acirc;́t là kh&ocirc;ng gian ngh&ecirc;̣ thu&acirc;̣t với 9 tác ph&acirc;̉m được tạo tác c&ocirc;ng phu, tỉ mỉ từ chocolate như: &ldquo;Bàn ti&ecirc;̣c hoàng gia&rdquo; với t&acirc;́t cả các v&acirc;̣t dụng (chén, bát, dĩa, mu&ocirc;̃ng, nĩa...) cùng đ&ocirc;̀ ăn, trái c&acirc;y, mặt bàn, C&acirc;y mai anh đào hay tác ph&acirc;̉m &ldquo;Su&ocirc;́i hoa&rdquo; tuyệt mỹ,&hellip; Nơi đ&acirc;y là kh&ocirc;ng gian thưởng lãm chocolate đ&ecirc;̉ cảm nh&acirc;̣n hương vị nguy&ecirc;n ch&acirc;́t h&ograve;a c&ugrave;ng sự đa dạng, phong phú và khác bi&ecirc;̣t từ tạo hình chocolate đ&ecirc;́n hương và sắc.<br />
N&ocirc;ng Trại C&uacute;n Puppy Farm: đến đ&acirc;y, du kh&aacute;ch thỏa th&iacute;ch tạo d&aacute;ng c&ugrave;ng những ch&uacute; c&uacute;n si&ecirc;u dễ thương hay chọn cho m&igrave;nh những g&oacute;c chụp đẹp b&ecirc;n c&aacute;c lo&agrave;i hoa với đủ m&agrave;u sắc rực rỡ, vườn b&iacute; ng&ocirc;, d&acirc;u t&acirc;y, c&agrave; chua bi,&hellip; tựa như l&agrave; một Đ&agrave; Lạt thu nhỏ, v&ocirc; c&ugrave;ng thơ mộng v&agrave; l&atilde;ng mạn.&nbsp;<br />
Buổi chiều, đo&agrave;n tiếp tục kh&aacute;m ph&aacute; những điểm đến mang đậm dấu ấn lịch sử một thời của v&ugrave;ng đất Đ&agrave; Lạt:<br />
Đồi ch&egrave; Cầu Đất: đến đ&acirc;y du kh&aacute;ch sẽ cảm nhận sự nhẹ nh&agrave;ng, b&igrave;nh y&ecirc;n bởi kh&ocirc;ng gian xanh mướt được bao phủ dọc theo c&aacute;c luống ch&egrave; dường như trải d&agrave;i bất tận. Check in giữa m&agrave;u v&agrave;ng nắng của c&aacute;nh đồng hoa cải đang v&agrave;o m&ugrave;a, sắc t&iacute;m mơ mộng của lo&agrave;i lavender,&hellip; (hoa theo m&ugrave;a)<br />
C&aacute;nh đồng hoa Cẩm T&uacute; Cầu: du kh&aacute;ch sẽ như cho&aacute;ng ngợp bởi c&aacute;nh đồng hoa Cẩm T&uacute; Cầu rộng lớn một m&agrave;u xanh biếc, nh&igrave;n từ xa thảm hoa như trải d&agrave;i bất tận. Nơi đ&acirc;y thu h&uacute;t du kh&aacute;ch bởi những g&oacute;c h&igrave;nh ảo diệu v&agrave; đặc biệt l&agrave; g&oacute;c chụp &ldquo;nấc thang l&ecirc;n thi&ecirc;n đường&rdquo;.<br />
Buổi tối, Qu&yacute; kh&aacute;ch tự do tản bộ, nh&acirc;m nhi c&aacute;c m&oacute;n ăn đường phố của Đ&agrave; Lạt như b&aacute;nh tr&aacute;ng nướng, sữa đậu n&agrave;nh Tăng Bạt Hổ hoặc thưởng thức những n&eacute;t nhạc xưa cũ tại ph&ograve;ng tr&agrave; Diễm Xưa. Nghỉ đ&ecirc;m tại Đ&agrave; Lạt.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 03<br />
21-06-2020<br />
&nbsp;&nbsp;&nbsp;Đ&Agrave; LẠT &ndash; M&Ecirc; LINH COFFEE - TP.HỒ CH&Iacute; MINH (Ăn s&aacute;ng, trưa)<br />
Sau khi d&ugrave;ng bữa s&aacute;ng, đo&agrave;n trả ph&ograve;ng v&agrave; tham quan:<br />
M&ecirc; Linh Coffee (chi ph&iacute; nước uống tự t&uacute;c): Được thiết kế mở, đảm bảo cho du kh&aacute;ch một tầm nh&igrave;n trọn vẹn Đ&agrave; Lạt 360 độ hướng ra trang trại c&agrave; ph&ecirc; bạt ng&agrave;n, đồi th&ocirc;ng, m&acirc;y trời v&agrave; mặt hồ đập Cam Ly y&ecirc;n b&igrave;nh. Tại đ&acirc;y, du kh&aacute;ch t&igrave;m hiểu c&aacute;ch thức sản xuất c&agrave; ph&ecirc; chồn trực tiếp hay thưởng thức ly c&agrave; ph&ecirc; chồn đậm đ&agrave;, mang vị đắng đặc trưng.<br />
Đo&agrave;n khởi h&agrave;nh về lại TP.Hồ Ch&iacute; Minh, trả kh&aacute;ch tại điểm đ&oacute;n ban đầu. Kết th&uacute;c h&agrave;nh tr&igrave;nh nhiều trải nghiệm.<br />
&nbsp;</p>
', N'04', 2, 1, 1, 1, 4, 0, CAST(N'2020-06-15T00:00:00.000' AS DateTime), CAST(N'2020-06-15T00:00:00.000' AS DateTime), 19)
INSERT [dbo].[tour] ([id], [ten], [hinhanh], [thoigian], [gia], [tomtat], [chitiet], [matour], [phuongtien_fk], [hienthi], [noibat], [nguoitao_fk], [diadiem_fk], [daxoa], [ngaytao], [ngaycapnhat], [giamgia]) VALUES (1007, N'Du lịch  Nha Trang - Khánh Hòa 3 ngày 2 đêm dành cho 4 người', N'/UserUpload/images/hinhtour1.jpg', CAST(N'2020-06-15T00:00:00.000' AS DateTime), 2600000, N'Tóm tắt chuyến đi du lịch  Nha Trang - Khánh Hòa 3 ngày 2 đêm', N'<p>TP.HCM - NHA TRANG &ndash;GALINA LAKE VIEW (Ăn trưa, tối)<br />
Qu&yacute; kh&aacute;ch tập trung tại ga đi Trong Nước S&acirc;n bay T&acirc;n Sơn Nhất, hướng dẫn vi&ecirc;n hỗ trợ l&agrave;m thủ tục cho đo&agrave;n đ&aacute;p chuyến bay đi Nha Trang. Đến S&acirc;n bay Cam Ranh, xe v&agrave; hướng dẫn vi&ecirc;n Vietravel đ&oacute;n đo&agrave;n đi tham quan:&nbsp;&nbsp;<br />
- Khu du lịch Galina Lake View: với một h&agrave;nh tr&igrave;nh 2 điểm đến, đến đ&acirc;y vừa tham quan Khu Di t&iacute;ch Lịch sử: &quot;Căn cứ C&aacute;ch Mạng Đồng B&ograve;&quot; vừa tham quan khung cảnh thơ mộng với con đường đầy hoa, những gi&agrave;n hoa giấy đỏ, những vườn c&acirc;y th&ocirc;ng hữu t&igrave;nh.<br />
Đo&agrave;n d&ugrave;ng bữa trưa tại khu du lịch, nhận ph&ograve;ng kh&aacute;ch sạn. Buổi chiều, đo&agrave;n tham quan:<br />
- Th&aacute;p B&agrave; Ponagar: c&ocirc;ng tr&igrave;nh ti&ecirc;u biểu cho nghệ thuật kiến tr&uacute;c v&agrave; đi&ecirc;u khắc t&iacute;n ngưỡng t&ocirc;n gi&aacute;o bậc nhất của d&acirc;n tộc Chăm tại Nha Trang.<br />
- L&agrave;ng Yến Mai Sinh: tham quan v&aacute;ch yến nh&acirc;n tạo lớn nhất Việt Nam, t&igrave;m hiểu quy tr&igrave;nh lấy yến ở độ cao cheo leo hiểm trở v&agrave; c&aacute;ch thức sơ chế để tạo ra sản phẩm dinh dưỡng cho sức khỏe.<br />
Buổi tối, Qu&yacute; kh&aacute;ch c&oacute; thể tự do trải nghiệm phố biển về đ&ecirc;m hay thưởng thức 1 ly cocktail b&ecirc;n cạnh hồ bơi của kh&aacute;ch sạn.&nbsp;<br />
&nbsp;<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 02<br />
20-06-2020<br />
&nbsp;&nbsp;&nbsp;NHA TRANG &ndash; VINPEARL LAND CẢ NG&Agrave;Y (Ăn s&aacute;ng, tự t&uacute;c ăn trưa v&agrave; tối)<br />
Sau khi d&ugrave;ng bữa s&aacute;ng tại kh&aacute;ch sạn, Qu&yacute; kh&aacute;ch c&oacute; thể tự do kh&aacute;m ph&aacute; những c&ocirc;ng tr&igrave;nh nổi tiếng của Nha Trang như Ch&ugrave;a Long Sơn, Nh&agrave; Thờ Đ&aacute;, đi chợ hoặc mua v&eacute; tham quan:<br />
- Thế Giới Vui Chơi Giải Tr&iacute; Vinpearl Land - Ngắm to&agrave;n cảnh Vịnh Nha Trang từ đỉnh v&ograve;ng xoay mặt trời lớn nhất Việt Nam, tham quan bộ sưu tập &ldquo;kỳ hoa, dị thảo&rdquo; tại đồi Vạn Hoa, khu vườn th&uacute; mở - vườn Qu&yacute; Vương, tham gia c&aacute;c tr&ograve; chơi tương t&aacute;c v&agrave; li&ecirc;n ho&agrave;n tại Quảng trường Thần Thoại hay V&ugrave;ng đất y&ecirc;u thương, xem phim 4D tại l&acirc;u đ&agrave;i Đại Dương, kh&aacute;m ph&aacute; khu tr&ograve; chơi cảm gi&aacute;c mạnh, khu tr&ograve; chơi trong nh&agrave; hay tắm biển, tắm hồ bơi lớn nhất Đ&ocirc;ng Nam &Aacute;&hellip; v&agrave; thưởng thức chương tr&igrave;nh biểu diễn Nhạc Nước hiện đại. Đo&agrave;n d&ugrave;ng bữa trưa tự do hoặc d&ugrave;ng buffet tại Vinpearl v&agrave; tiếp tục tham gia c&aacute;c hoạt động buổi chiều tại Vinpearl.<br />
&nbsp;<br />
Buổi tối, Qu&yacute; kh&aacute;ch c&oacute; thể tự do thưởng thức hải sản tại Phố Th&aacute;p B&agrave; hay những nh&agrave; h&agrave;ng dọc đường Trần Ph&uacute; v&agrave; thư gi&atilde;n với massage ch&acirc;n xua tan mệt mỏi.<br />
&nbsp;<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 03<br />
21-06-2020<br />
&nbsp;&nbsp;&nbsp;NHA TRANG &ndash; NHŨ TI&Ecirc;N &ndash; BẢO T&Agrave;NG GIẠC MA - TP.HCM (Ăn s&aacute;ng, trưa)<br />
Sau khi d&ugrave;ng bữa s&aacute;ng, đo&agrave;n khởi h&agrave;nh tham quan:<br />
- Khu du lịch biển Nhũ Ti&ecirc;n: Lưu dấu ch&acirc;n tr&ecirc;n bờ c&aacute;t d&agrave;i trắng mịn c&ugrave;ng l&agrave;n nước trong xanh, với đồi n&uacute;i tr&ugrave;ng điệp thu gọn trong tầm mắt lu&ocirc;n l&agrave; trải nghiệm tuyệt vời.<br />
- Bảo t&agrave;ng Gạc Ma: được biết đến như một &ldquo;địa chỉ đỏ&rdquo; gi&aacute;o dục chủ quyền biển đảo cho thế hệ trẻ, nổi bật với tương đ&agrave;i chiến sĩ mang chủ đề &ldquo;Những người nằm lại ph&iacute;a ch&acirc;n trời&rdquo; c&ugrave;ng c&aacute;c c&ocirc;ng tr&igrave;nh đi&ecirc;u khắc nghệ thuật sinh động, ấn tượng sẽ cho du kh&aacute;ch những th&ocirc;ng tin lịch sử qu&yacute; b&aacute;u c&ugrave;ng l&ograve;ng y&ecirc;u nước nồng n&agrave;n.<br />
&nbsp;Đo&agrave;n d&ugrave;ng cơm trưa, trả ph&ograve;ng v&agrave; khởi h&agrave;nh ra s&acirc;n bay Cam Ranh, l&agrave;m thủ tục bay về lại TP.HCM, Đến s&acirc;n bay T&acirc;n Sơn Nhất, chia tay v&agrave; kết th&uacute;c h&agrave;nh tr&igrave;nh nhiều trải nghiệm.<br />
&nbsp;<br />
&nbsp;</p>
', N'05', 1, 1, 1, 1, 1, 0, CAST(N'2020-06-15T00:00:00.000' AS DateTime), CAST(N'2020-06-15T00:00:00.000' AS DateTime), 18)
INSERT [dbo].[tour] ([id], [ten], [hinhanh], [thoigian], [gia], [tomtat], [chitiet], [matour], [phuongtien_fk], [hienthi], [noibat], [nguoitao_fk], [diadiem_fk], [daxoa], [ngaytao], [ngaycapnhat], [giamgia]) VALUES (1008, N'Du lịch 3 ngày 2 đêm Đà Nẵng - Hội An dành cho 3 người', N'/UserUpload/images/7.jpg', NULL, 319001, N'Du lịch 3 ngày 2 đêm Đà Nẵng - Hội An', N'<p>S&aacute;ng Qu&yacute; kh&aacute;ch tập trung Ga đi trong nước, s&acirc;n bay T&acirc;n Sơn Nhất. Hướng dẫn vi&ecirc;n hỗ trợ l&agrave;m thủ tục cho đo&agrave;n đ&aacute;p chuyến bay đi Đ&agrave; Nẵng. Tại s&acirc;n bay Đ&agrave; Nẵng:&nbsp;<br />
- B&aacute;n đảo Sơn Tr&agrave; v&agrave; Viếng Ch&ugrave;a Linh Ứng: Nơi đ&acirc;y c&oacute; tượng Phật Quan Thế &Acirc;m cao nhất Việt Nam, đứng nơi đ&acirc;y, Qu&yacute; kh&aacute;ch sẽ được chi&ecirc;m ngưỡng to&agrave;n cảnh th&agrave;nh phố, n&uacute;i rừng v&agrave; biển đảo Sơn Tr&agrave; một c&aacute;ch ho&agrave;n hảo nhất.&nbsp;<br />
- L&agrave;ng Rau Tr&agrave; Quế: Trải nghiệm tuyệt vời về quy tr&igrave;nh trồng rau l&acirc;u đời của người d&acirc;n Tr&agrave; Quế v&agrave; thưởng thức ly nước hạt &eacute; d&acirc;n d&atilde; gi&uacute;p xua tan mệt mỏi. Chắc chắn đ&acirc;y l&agrave; điểm tham quan trải nghiệm l&yacute; th&uacute; trong h&agrave;nh tr&igrave;nh du lịch đến với th&agrave;nh phố Hội An&nbsp;<br />
- Phố Cổ Hội An: Ch&ugrave;a Cầu, Nh&agrave; Cổ Ph&ugrave;ng Hưng, Hội Qu&aacute;n Phước Kiến, Cơ sở Thủ C&ocirc;ng Mỹ Nghệ,&hellip;.&nbsp;<br />
Buổi tối qu&yacute; kh&aacute;ch tự do dạo phố cổ ngắm đ&egrave;n lồng đầy m&agrave;u sắc, cảm nhận sự y&ecirc;n b&igrave;nh cổ k&iacute;nh v&agrave; l&atilde;ng mạn Phố Cổ về đ&ecirc;m,.. Nghỉ đ&ecirc;m tại Hội An.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 02<br />
09-07-2020<br />
&nbsp;&nbsp;&nbsp;HỘI AN &ndash; C&Ugrave; LAO CH&Agrave;M &ndash; Đ&Agrave; NẴNG Số bữa ăn: 02 (Ăn s&aacute;ng, tối)<br />
&nbsp;D&ugrave;ng bữa s&aacute;ng tại kh&aacute;ch sạn. Qu&yacute; kh&aacute;ch tự do đi tham quan kh&aacute;m ph&aacute; Quảng Nam theo sở th&iacute;ch:<br />
Lựa chọn 1:&nbsp;Xe v&agrave; HDV Vietravel đưa Qu&yacute; kh&aacute;ch đến Cảng Cửa Đại &ndash; Quảng Nam l&ecirc;n cano đi tham quan:&nbsp;&nbsp;<br />
- C&ugrave; Lao Ch&agrave;m: Khu dự trữ sinh quyển thế giới được UNESCO c&ocirc;ng nhận, với Khu Bảo T&agrave;ng, Ch&ugrave;a Hải Tạng, &Acirc;u Thuyền,... đi dạo t&igrave;m hiểu đời sống của cư d&acirc;n tr&ecirc;n đảo. Qu&yacute; kh&aacute;ch đắm m&igrave;nh trong l&agrave;n nước biển xanh m&aacute;t với b&atilde;i c&aacute;t trắng mịn, tự do thư gi&atilde;n tắm biển hoặc đi lặn ngắm san h&ocirc; bằng mặt nạ v&agrave; ống thở nhựa. Ăn trưa tr&ecirc;n đảo. Chiều cano đưa Qu&yacute; kh&aacute;ch trở lại đất liền. (Chi ph&iacute; Cano C&ugrave; Lao Ch&agrave;m v&agrave; Ăn trưa tr&ecirc;n đảo tự t&uacute;c)&nbsp;<br />
Lựa chọn 2: Qu&yacute; kh&aacute;ch ở lại kh&aacute;ch sạn/resort tự do nghỉ ngơi, hoặc đi dạo phố cổ Hội An, cảm nhận sự y&ecirc;n b&igrave;nh, kh&ocirc;ng kh&iacute; trong l&agrave;nh Hội An v&agrave;o buổi s&aacute;ng. Tự do thưởng thức đặc sản: B&aacute;nh M&igrave; Phượng, Cao Lầu,M&igrave; Quảng, B&aacute;nh Đập.. Hoặc đi tham quan v&agrave; kh&aacute;m ph&aacute; Rừng Dừa Bảy Mẫu hoặc khu vui chơi Vinpearl Land Nam Hội An,.. (chi ph&iacute; tự t&uacute;c). Khoảng 15:00 Xe đ&oacute;n đo&agrave;n c&ugrave;ng khởi h&agrave;nh về Đ&agrave; Nẵng tham quan danh thắng:<br />
- Ngũ H&agrave;nh Sơn: Động&nbsp; T&agrave;ng Chơn, Động Hoa Nghi&ecirc;m, Ch&ugrave;a Non Nước, L&agrave;ng Đ&aacute; Mỹ Nghệ,..<br />
Buổi tối Qu&yacute; kh&aacute;ch tự do dạo phố, thưởng thức đặc sản địa phương; thưởng ngoạn cảnh đẹp của Đ&agrave; Nẵng về đ&ecirc;m, ngắm nh&igrave;n Cầu Rồng, Cầu T&igrave;nh Y&ecirc;u, Cầu Trần Thị L&yacute;, Trung T&acirc;m Thương Mại, Khu phố ẩm thực, Caf&eacute; - Bar - Disco&hellip; hoặc đi tham quan C&ocirc;ng vi&ecirc;n ch&acirc;u &Aacute; -&nbsp; Asian Park (Chi ph&iacute; tự t&uacute;c). Nghỉ đ&ecirc;m tại Đ&agrave; Nẵng.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 03<br />
10-07-2020<br />
&nbsp;&nbsp;&nbsp;Đ&Agrave; NẴNG &ndash; B&Agrave; N&Agrave; &ndash; CẦU V&Agrave;NG - TP. HỒ CH&Iacute; MINH Số bữa ăn: 01 (Ăn s&aacute;ng)<br />
D&ugrave;ng bữa s&aacute;ng tại kh&aacute;ch sạn. Xe đưa Qu&yacute; kh&aacute;ch đi tham quan:<br />
- Khu du lịch B&agrave; N&agrave; - Suối Mơ (chi ph&iacute; c&aacute;p treo &amp; ăn trưa tự t&uacute;c) : Qu&yacute; kh&aacute;ch tự do tận hưởng kh&ocirc;ng kh&iacute; se lạnh của Đ&agrave; Lạt tại miền Trung, đo&agrave;n tự do tham quan Ch&ugrave;a Linh Ứng, Hầm Rượu Debay, vườn hoa Le Jardin D&rsquo;Amour, Khu T&acirc;m linh mới của B&agrave; N&agrave; viếng Đền Lĩnh Ch&uacute;a Linh Từ, khu vui chơi Fantasy Park, tự do chụp h&igrave;nh tại Cầu V&agrave;ng điểm tham quan mới si&ecirc;u hot tại B&agrave; N&agrave;&hellip;Ăn trưa tại B&agrave; N&agrave;. Sau đ&oacute; đo&agrave;n tiếp tục tham quan vui chơi đến giờ xuống c&aacute;p.&nbsp;<br />
Xe v&agrave; hướng dẫn vi&ecirc;n Vietravel tiễn Qu&yacute; kh&aacute;ch ra s&acirc;n bay Đ&agrave; Nẵng đ&oacute;n chuyến bay trở về Tp.Hồ Ch&iacute; Minh. Chia tay Qu&yacute; kh&aacute;ch v&agrave; kết th&uacute;c chương tr&igrave;nh du lịch tại s&acirc;n bay T&acirc;n Sơn Nhất.&nbsp;&nbsp;<br />
&nbsp;</p>

<div><img src="chrome-extension://bpggmmljdiliancllaapiggllnkbjocb/icon/16.png" /></div>
', N'06', 1, 1, 1, 1, 5, 0, NULL, NULL, 15)
INSERT [dbo].[tour] ([id], [ten], [hinhanh], [thoigian], [gia], [tomtat], [chitiet], [matour], [phuongtien_fk], [hienthi], [noibat], [nguoitao_fk], [diadiem_fk], [daxoa], [ngaytao], [ngaycapnhat], [giamgia]) VALUES (1009, N'Tour du lịch 5 ngày 4 đêm Đà Nẵng - Hội An - Hếu dành cho 5 người', N'/UserUpload/images/8.jpg', NULL, 589000, N'Tour du lịch 5 ngày 4 đêm Đà Nẵng - Hội An - Hếu', N'<p>Qu&yacute; kh&aacute;ch tập trung tại Ga đi trong nước, s&acirc;n bay T&acirc;n Sơn Nhất, hướng dẫn vi&ecirc;n Vietravel hỗ trợ l&agrave;m thủ tục cho đo&agrave;n đ&aacute;p chuyến bay đi Huế. Tại s&acirc;n bay Ph&uacute; B&agrave;i - Huế, xe v&agrave; hướng dẫn vi&ecirc;n đ&oacute;n đo&agrave;n đi tham quan:&nbsp;&nbsp;<br />
- Đại Nội: ho&agrave;ng cung xưa của 13 vị vua triều Nguyễn, tham quan Ngọ M&ocirc;n, Điện Th&aacute;i H&ograve;a, Tử Cấm Th&agrave;nh, Thế Miếu, Hiển L&acirc;m C&aacute;c, Cửu Đ&igrave;nh, Bảo t&agrave;ng Cổ Vật cung đ&igrave;nh Huế.&nbsp;<br />
- Ch&ugrave;a Thi&ecirc;n Mụ: ng&ocirc;i ch&ugrave;a được xem l&agrave; cổ nhất ở Huế v&agrave; l&agrave; nơi lưu giữ nhiều cổ vật qu&yacute; gi&aacute; kh&ocirc;ng chỉ về mặt lịch sử m&agrave; c&ograve;n cả về nghệ thuật<br />
- Lăng Tự Đức: nơi c&oacute; phong cảnh sơn thủy hữu t&igrave;nh v&agrave; được cho l&agrave; một trong những lăng tẩm c&oacute; kiến tr&uacute;c đẹp nhất của c&aacute;c vua ch&uacute;a nh&agrave; Nguyễn&nbsp;<br />
- Dạo Phố Đ&ecirc;m: chụp h&igrave;nh tại Cầu Đi Bộ S&agrave;n Gỗ Lim tr&ecirc;n S&ocirc;ng Hương điểm tham quan mới si&ecirc;u h&oacute;t tại Huế, ,cầu Trường Tiền, tranh th&ecirc;u XQ, tự do thưởng thức c&aacute;c m&oacute;n đường phố xứ Huế, ngắm nh&igrave;n thuyền rồng ngược xu&ocirc;i b&ecirc;n bến T&ograve;a Kh&acirc;m văng vẳng &acirc;m vang điệu h&ograve; Huế, kh&aacute;m ph&aacute; khu phố T&acirc;y s&ocirc;i động về đ&ecirc;m.<br />
- Kỳ đ&agrave;i Huế: thắp s&aacute;ng Kỳ Đ&agrave;i Huế l&agrave; sự kiện thuộc chuỗi dự &aacute;n &ldquo;Huế - S&aacute;ng v&agrave; Sống&rdquo; do Vietravel đầu tư v&agrave; phối hợp thực hiện c&ugrave;ng UBND tỉnh Thừa Thi&ecirc;n - Huế.&nbsp;<br />
Về lại kh&aacute;ch sạn, nhận ph&ograve;ng tự do nghỉ ngơi. Nghỉ đ&ecirc;m tại Huế<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 02<br />
09-07-2020<br />
&nbsp;&nbsp;&nbsp;HUẾ - ĐỘNG PHONG NHA - ĐỘNG THI&Ecirc;N ĐƯỜNG Số bữa ăn: 03 (Ăn s&aacute;ng, trưa, chiều)<br />
Ăn s&aacute;ng buffet tại kh&aacute;ch sạn. Trả ph&ograve;ng, Qu&yacute; kh&aacute;ch khởi h&agrave;nh đi Quảng B&igrave;nh tham quan:<br />
- Động Phong Nha: một nh&aacute;nh trong quần thề di sản thi&ecirc;n nhi&ecirc;n thế giới, được xem như chốn thần ti&ecirc;n bởi hệ thống n&uacute;i đ&aacute; v&ocirc;i v&agrave; s&ocirc;ng ngầm d&agrave;i nhất thế giới,&hellip;&nbsp;&nbsp;<br />
- Động Thi&ecirc;n Đường: như l&agrave; ho&agrave;ng cung dưới l&ograve;ng đất, hang động đ&aacute; v&ocirc;i đẹp v&agrave; kỳ ảo, c&oacute; độ d&agrave;i kỷ lục,&hellip;&nbsp;<br />
Buổi tối Qu&yacute; kh&aacute;ch tự do nghỉ ngơi, dạo phố kh&aacute;m ph&aacute; th&agrave;nh phố Đồng Hới về đ&ecirc;m. Nghỉ đ&ecirc;m tại Quảng B&igrave;nh.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 03<br />
10-07-2020<br />
&nbsp;&nbsp;&nbsp;QUẢNG B&Igrave;NH - LA VANG - Đ&Agrave; NẴNG Số bữa ăn: 03 (Ăn s&aacute;ng, trưa, chiều)<br />
Ăn s&aacute;ng buffet tại kh&aacute;ch sạn. Qu&yacute; kh&aacute;ch khởi h&agrave;nh về Đ&agrave; Nẵng, tr&ecirc;n đường đi dừng tham quan:<br />
- Th&agrave;nh cổ Quảng Trị: nghe hướng dẫn vi&ecirc;n thuyết minh Khu phi qu&acirc;n sự DMZ v&agrave; Cầu Hiền Lương S&ocirc;ng Bến Hải (Vĩ tuyến 17)<br />
- Th&aacute;nh địa La Vang: một trong bốn tiểu vương cung th&aacute;nh đường La M&atilde; tại Việt Nam<br />
- Đầm Lập An: ngắm cảnh m&acirc;y bồng bềnh tr&ecirc;n những ch&oacute;p n&uacute;i bao bọc quanh đầm, trước khi đi xuy&ecirc;n Hầm Hải V&acirc;n đến Đ&agrave; Nẵng&nbsp;<br />
Buổi tối Qu&yacute; kh&aacute;ch tự t&uacute;c đi dạo phố thưởng ngoạn cảnh đẹp của Đ&agrave; Nẵng về đ&ecirc;m, ngắm nh&igrave;n Cầu Rồng, Cầu T&igrave;nh Y&ecirc;u, Cầu Trần Thị L&yacute;, Trung T&acirc;m Thương Mại, Khu phố ẩm thực, Caf&eacute; - Bar - Disco&hellip;. Nghỉ đ&ecirc;m tại Đ&agrave; Nẵng.<br />
&nbsp;<br />
Ghi ch&uacute;: Giai đoạn từ 08/08 &ndash; 16/08/2019, Th&aacute;nh Địa La Vang diễn ra &quot;Đại Hội H&agrave;nh Hương Đức Mẹ La Vang&quot; h&agrave;ng năm, n&ecirc;n c&aacute;c tour khởi h&agrave;nh trong giai đoạn n&agrave;y sẽ kh&ocirc;ng v&agrave;o tham quan La Vang)<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 04<br />
11-07-2020<br />
&nbsp;&nbsp;&nbsp;Đ&Agrave; NẴNG - B&Agrave; N&Agrave; (CẦU V&Agrave;NG) - HỘI AN Số bữa ăn: 02 (Ăn s&aacute;ng, chiều)<br />
Ăn s&aacute;ng buffet tại kh&aacute;ch sạn. Xe đưa Qu&yacute; kh&aacute;ch đi tham quan:&nbsp;<br />
- Khu du lịch B&agrave; N&agrave; Suối Mơ (Chi ph&iacute; c&aacute;p treo &amp; ăn trưa tự t&uacute;c): trải nghiệm tuyến c&aacute;p treo một d&acirc;y d&agrave;i nhất thế giới. L&ecirc;n đỉnh n&uacute;i, Qu&yacute; kh&aacute;ch tự do tận hưởng kh&ocirc;ng kh&iacute; se lạnh của &ldquo;Đ&agrave; Lạt tại miền Trung&rsquo;&rsquo; tham quan&nbsp; Ch&ugrave;a Linh Ứng, vườn hoa Le Jardin D&rsquo;Amour, khu t&acirc;m linh mới của B&agrave; N&agrave; viếng Đền Lĩnh Ch&uacute;a Linh Từ, khu vui chơi Fantasy Park với nhiều tr&ograve; chơi hấp dẫn, đi dạo chụp h&igrave;nh tại Cầu V&agrave;ng điểm tham quan mới si&ecirc;u hot tại B&agrave; N&agrave;, Ăn trưa tại B&agrave; N&agrave; tự t&uacute;c. Tự do vui chơi đến giờ xuống c&aacute;p.&nbsp; &nbsp;<br />
- Phố Cổ Hội An: Ch&ugrave;a Cầu, dạo phố đ&egrave;n lồng đầy m&agrave;u sắc, cảm nhận sự y&ecirc;n b&igrave;nh, cổ k&iacute;nh, l&atilde;ng mạn phố cổ, tự do thả hoa đăng cầu b&igrave;nh an tr&ecirc;n S&ocirc;ng Ho&agrave;i,&hellip;&nbsp;<br />
Đo&agrave;n về lại Đ&agrave; Nẵng, nhận ph&ograve;ng tự do nghỉ ngơi. Nghỉ đ&ecirc;m tại Đ&agrave; Nẵng<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 05<br />
12-07-2020<br />
&nbsp;&nbsp;&nbsp;Đ&Agrave; NẴNG - SƠN TR&Agrave; - TP.HỒ CH&Iacute; MINH Số bữa ăn: 02 (Ăn s&aacute;ng, trưa)<br />
Ăn s&aacute;ng buffet tại kh&aacute;ch sạn. Xe đưa Qu&yacute; kh&aacute;ch đi tham quan:&nbsp;<br />
- B&atilde;i biển Mỹ Kh&ecirc;: một trong những b&atilde;i biển quyến rũ nhất h&agrave;nh tinh, Qu&yacute; kh&aacute;ch tự do dạo biển, chụp h&igrave;nh.&nbsp;<br />
- B&aacute;n đảo Sơn Tr&agrave; v&agrave; Viếng Ch&ugrave;a Linh Ứng: nơi đ&acirc;y c&oacute; tượng Phật Quan Thế &Acirc;m cao nhất Việt Nam. Đứng nơi đ&acirc;y, Qu&yacute; kh&aacute;ch sẽ được chi&ecirc;m ngưỡng to&agrave;n cảnh th&agrave;nh phố, n&uacute;i rừng v&agrave; biển đảo Sơn Tr&agrave; một c&aacute;ch ho&agrave;n hảo nhất<br />
- Ngũ H&agrave;nh Sơn: Động T&agrave;ng Chơn, Động Hoa Nghi&ecirc;m, Ch&ugrave;a Non Nước, L&agrave;ng Đ&aacute; Mỹ Nghệ Non Nước,..<br />
- Ky niem local specialites and souvernia shop: mua sắm đặc sản của Đ&agrave; Nẵng v&agrave; c&aacute;c v&ugrave;ng miền&nbsp; &nbsp;<br />
Xe đưa đo&agrave;n ra s&acirc;n bay Đ&agrave; Nẵng đ&oacute;n chuyến bay trở về Tp.Hồ Ch&iacute; Minh. Chia tay đo&agrave;n v&agrave; kết th&uacute;c chương tr&igrave;nh du lịch tại s&acirc;n bay T&acirc;n Sơn Nhất.<br />
&nbsp;</p>

<div class="ddict_div" style="top: 1530px; max-width: 809.766px; left: 14.8828px;">
<p class="ddict_sentence">&nbsp;</p>
</div>
', N'07', 1, 1, 1, 1, 5, 0, NULL, NULL, 10)
INSERT [dbo].[tour] ([id], [ten], [hinhanh], [thoigian], [gia], [tomtat], [chitiet], [matour], [phuongtien_fk], [hienthi], [noibat], [nguoitao_fk], [diadiem_fk], [daxoa], [ngaytao], [ngaycapnhat], [giamgia]) VALUES (1010, N'Tour du lịch 5 ngày 4 đêm Hà Nội - Yến Tử - Hạ Long dành cho 7 người', N'/UserUpload/images/9.jpg', NULL, 8200000, N'Tour du lịch 5 ngày 4 đêm Hà Nội - Yến Tử - Hạ Long', N'<p>&nbsp; &nbsp;TP. HỒ CH&Iacute; MINH - NỘI B&Agrave;I (H&Agrave; NỘI) - L&Agrave;O CAI &ndash; SAPA (Ăn trưa, chiều)<br />
Qu&yacute; kh&aacute;ch tập trung tại s&acirc;n bay T&acirc;n Sơn Nhất - ga đi trong nước. Hướng dẫn vi&ecirc;n l&agrave;m thủ tục cho đo&agrave;n đ&aacute;p chuyến bay đi H&agrave; Nội.<br />
Xe Vietravel đ&oacute;n đo&agrave;n tại s&acirc;n bay Nội B&agrave;i, khởi h&agrave;nh đi Sa Pa theo cung đường cao tốc hiện đại v&agrave; d&agrave;i nhất Việt Nam. Tr&ecirc;n đường dừng tham quan Đền Quốc Tổ Lạc Long Qu&acirc;n được tọa lạc tr&ecirc;n đồi Sim c&oacute; h&igrave;nh thể giống một con r&ugrave;a lớn, hai b&ecirc;n c&oacute; Thanh Long, Bạch Hổ, ph&iacute;a trước l&agrave; hồ H&oacute;c Trai v&agrave; s&ocirc;ng Hồng chảy xu&ocirc;i về biển, biểu hiện sự linh thi&ecirc;ng, huyền b&iacute; với thế &quot;sơn chầu thủy tụ&quot; v&ocirc; c&ugrave;ng đắc địa.<br />
Đến Sapa Qu&yacute; kh&aacute;ch tham quan:<br />
- Bản C&aacute;t C&aacute;t đẹp như một bức tranh giữa v&ugrave;ng phố cổ Sapa, nơi đ&acirc;y thu h&uacute;t du kh&aacute;ch bởi cầu treo, th&aacute;c nước, guồng nước v&agrave; những mảng m&agrave;u hoa m&ecirc; hoặc du kh&aacute;ch khi lạc bước đến đ&acirc;y.<br />
- Thăm những nếp nh&agrave; của người M&ocirc;ng, Dao, Gi&aacute;y trong bản, du kh&aacute;ch sẽ kh&ocirc;ng khỏi ngỡ ng&agrave;ng trước vẻ đẹp b&igrave;nh dị, mộc mạc.&nbsp;<br />
Buổi tối Qu&yacute; kh&aacute;ch dạo phố, ngắm nh&agrave; thờ Đ&aacute; Sapa, tự do thưởng thức đặc sản v&ugrave;ng cao như: thịt lợn cấp n&aacute;ch nướng, trứng nướng, rượu t&aacute;o m&egrave;o, giao lưu với người d&acirc;n tộc v&ugrave;ng cao.&nbsp;<br />
Nghỉ đ&ecirc;m tại Sapa.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 02<br />
08-07-2020<br />
&nbsp;&nbsp;&nbsp;SA PA &ndash; FANSIPAN H&Ugrave;NG VĨ - L&Agrave;O CAI (Ăn s&aacute;ng, trưa, chiều)<br />
Trả ph&ograve;ng kh&aacute;ch sạn, xe đưa đo&agrave;n ra ga Sapa, Qu&yacute; kh&aacute;ch trải nghiệm đến khu du lịch Fansipan Legend bằng T&agrave;u hỏa leo n&uacute;i Mường Hoa hiện đại nhất Việt Nam với tổng chiều d&agrave;i gần 2000m, thưởng ngoạn bức tranh phong cảnh đầy m&agrave;u sắc của c&aacute;nh rừng nguy&ecirc;n sinh, thung lũng Mường Hoa.<br />
- Chinh phục đỉnh n&uacute;i Fansipan với độ cao 3.143m h&ugrave;ng vĩ bằng c&aacute;p treo (chi ph&iacute; tự t&uacute;c).&nbsp;<br />
- Lễ Phật tại ch&ugrave;a Tr&igrave;nh hay cầu ph&uacute;c lộc, b&igrave;nh an cho gia đ&igrave;nh tại B&iacute;ch V&acirc;n Thiền Tự trong hệ thống cảnh quan t&acirc;m linh tr&ecirc;n đỉnh Fansipan.&nbsp;<br />
Buổi chiều tham quan:<br />
- N&uacute;i H&agrave;m Rồng thăm vườn Lan khoe sắc, vườn hoa Trung T&acirc;m, ngắm N&uacute;i Phanxipăng h&ugrave;ng vĩ, Cổng Trời, Đầu Rồng Thạch L&acirc;m, S&acirc;n M&acirc;y.&nbsp;&nbsp;<br />
Trong trường hợp kh&ocirc;ng tham quan được Hàm R&ocirc;̀ng vì lí do b&acirc;́t khả kháng, chương trình sẽ được&nbsp; &nbsp;chuy&ecirc;̉n sang Thác Bạc.<br />
Nghỉ đ&ecirc;m Sapa.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 03<br />
09-07-2020<br />
&nbsp;&nbsp;&nbsp;L&Agrave;O CAI - H&Agrave; NỘI (Ăn s&aacute;ng, trưa, chiều)<br />
Trả ph&ograve;ng kh&aacute;ch sạn, qu&yacute; kh&aacute;ch tham quan&nbsp;<br />
- Cửa khẩu bi&ecirc;n giới Việt - Trung &ldquo;L&agrave;o Cai- H&agrave; Khẩu&rdquo;<br />
- Mua sắm tại chợ Cốc Lếu - Trung t&acirc;m thương mại lớn nhất, của th&agrave;nh phố n&oacute;i ri&ecirc;ng v&agrave; Tỉnh L&agrave;o Cai n&oacute;i chung. Nơi đ&acirc;y b&agrave;y b&aacute;n đa dạng đủ c&aacute;c loại mặt h&agrave;ng từ thủ c&ocirc;ng mỹ nghệ, tranh nghệ thuật phong cảnh đến quần &aacute;o&hellip;sẽ l&agrave; một điểm mua sắm tuyệt vời với du kh&aacute;ch.<br />
Theo cung đường cao tốc trở về H&agrave; Nội, nhận ph&ograve;ng nghỉ ngơi. Xe đưa Qu&yacute; kh&aacute;ch dạo quanh&nbsp;<br />
- Hồ Ho&agrave;n Kiếm ngắm Th&aacute;p R&ugrave;a, Đền Ngọc Sơn, Cầu Th&ecirc; H&uacute;c.&nbsp;<br />
Sau bữa ăn chiều, Qu&yacute; Kh&aacute;ch tự do kh&aacute;m ph&aacute; H&agrave; Nội hoặc lựa chọn chương tr&igrave;nh thư gi&atilde;n (chi ph&iacute; tự t&uacute;c):<br />
- Trải nghiệm massage bấm huyệt kiểu Việt Nam truyền thống tại Spa Sen Garden, &iacute;t nhất 75 ph&uacute;t gồm c&aacute;c c&ocirc;ng đoạn tắm trắng, massage, bấm huyệt, sau đ&oacute; d&ugrave;ng bữa nhẹ buffet mini gồm 10 m&oacute;n nhẹ + nước uống, ch&egrave;.<br />
Nghỉ đ&ecirc;m tại H&agrave; Nội.&nbsp;<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 04<br />
10-07-2020<br />
&nbsp;&nbsp;&nbsp;H&Agrave; NỘI - Y&Ecirc;N TỬ - HẠ LONG (Ăn s&aacute;ng, trưa, chiều)<br />
Sau khi ăn s&aacute;ng, xe đưa Qu&yacute; kh&aacute;ch tham quan<br />
- Lăng Hồ Chủ Tịch &ndash; t&igrave;m hiểu về cuộc đời v&agrave; sự nghiệp của vị cha gi&agrave; k&iacute;nh y&ecirc;u của d&acirc;n tộc (kh&ocirc;ng viếng v&agrave;o thứ 2, thứ 6 h&agrave;ng tuần v&agrave; giai đoạn bảo tr&igrave; định k&igrave; h&agrave;ng năm 15/6 &ndash; 15/8), Nh&agrave; S&agrave;n B&aacute;c Hồ, Bảo T&agrave;ng Hồ Ch&iacute; Minh, Ch&ugrave;a Một Cột.<br />
- Văn Miếu - Nơi thờ Khổng Tử v&agrave; c&aacute;c bậc hiền triết của Nho Gi&aacute;o, Quốc Tử Gi&aacute;m - Trường đại học đầu ti&ecirc;n của Việt Nam, t&igrave;m về cội nguồn lịch sử của c&aacute;c vị Nho học.<br />
Tiếp tục h&agrave;nh tr&igrave;nh đi Hạ Long, tr&ecirc;n đường đi Qu&yacute; kh&aacute;ch gh&eacute; tham quan<br />
- N&uacute;i Y&ecirc;n Tử - Thắng cảnh thi&ecirc;n nhi&ecirc;n c&ograve;n lưu giữ nhiều di t&iacute;ch lịch sử với mệnh danh &ldquo;Đất tổ Phật gi&aacute;o Việt Nam&rdquo;.&nbsp;<br />
- Ch&ugrave;a Hoa Y&ecirc;n - Ng&ocirc;i ch&ugrave;a to v&agrave; đẹp, nằm tr&ecirc;n lưng chừng n&uacute;i ở độ cao 516m, thăm Th&aacute;p Tổ (chi ph&iacute;&nbsp;<br />
c&aacute;p treo tự t&uacute;c).&nbsp;<br />
Chiều về Hạ Long nhận ph&ograve;ng. Buổi tối xe đưa Qu&yacute; kh&aacute;ch ngắm cầu B&atilde;i Ch&aacute;y v&agrave; th&agrave;nh phố Hạ Long về đ&ecirc;m.<br />
Nghỉ đ&ecirc;m tại Hạ Long.&nbsp;<br />
Một số lựa chọn d&agrave;nh cho Qu&yacute; kh&aacute;ch muốn kh&aacute;m ph&aacute; th&ecirc;m Hạ Long (chi ph&iacute; tự t&uacute;c)<br />
- Lựa chọn 1:&nbsp;Tham quan Sun World Ha Long Park tr&ecirc;n N&uacute;i Ba Đ&egrave;o bằng c&aacute;p treo Nữ Ho&agrave;ng, trải nghiệm v&ograve;ng quay mặt trời, tham quan Cầu Koi giữa vườn Nhật, khu trưng b&agrave;y tượng s&aacute;p, khu vui chơi trẻ em.<br />
- Lựa chọn 2: trải nghiệm liệu tr&igrave;nh chăm s&oacute;c sức khỏe v&agrave; sắc đẹp bằng nước kho&aacute;ng n&oacute;ng tại Yoko&nbsp; Onsen Quang Hanh - Tổ hợp nghỉ dưỡng kho&aacute;ng n&oacute;ng 5 sao phong c&aacute;ch Nhật Bản được bao bọc giữa những ngọn n&uacute;i thường xanh c&ugrave;ng m&agrave;u thi&ecirc;n thanh của m&acirc;y trời, c&acirc;y cỏ v&agrave; d&ograve;ng chảy nước n&oacute;ng mang đầy kho&aacute;ng chất thi&ecirc;n nhi&ecirc;n. Bước ch&acirc;n v&agrave;o khu vườn Nhật đậm chất thiền tại Yoko Onsen Quang Hanh, Qu&yacute; kh&aacute;ch sẽ c&oacute; những trải nghiệm tuyệt vời với c&aacute;c dịch vụ đẳng cấp:<br />
&middot; Khu vực tắm chung c&oacute; 27 bể tắm kho&aacute;ng đa dạng: bể carbonate, bể tắm hang, bể tắm đ&aacute;, bể tắm đ&agrave;o vi&ecirc;n, bể tắm chum, bể sục, bể tắm trẻ em&hellip; ; Trải nghiệm x&ocirc;ng đ&aacute; kiểu Nhật (x&ocirc;ng n&oacute;ng, x&ocirc;ng lạnh, x&ocirc;ng đ&aacute; muối Himalaya&hellip;)&hellip; mang đến những c&ocirc;ng dụng chăm s&oacute;c sức khỏe đa dạng như đ&agrave;o thải độc tố, th&uacute;c đẩy tuần ho&agrave;n m&aacute;u, giảm stress, hỗ trợ điều trị c&aacute;c bệnh về da v&agrave; trẻ h&oacute;a l&agrave;n da&hellip;<br />
&middot; Trải nghiệm ph&ograve;ng nghỉ ngơi c&ocirc;ng cộng, ph&ograve;ng đọc s&aacute;ch, ph&ograve;ng chiếu phim<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 05<br />
11-07-2020<br />
&nbsp;&nbsp;&nbsp;HẠ LONG - NINH B&Igrave;NH (Ăn s&aacute;ng, trưa, chiều)<br />
Qu&yacute; kh&aacute;ch ra bến t&agrave;u, xuống t&agrave;u du ngoạn<br />
- Vịnh Hạ Long - Thắng cảnh thi&ecirc;n nhi&ecirc;n tuyệt đẹp v&agrave; v&ocirc; c&ugrave;ng sống động, được UNESCO c&ocirc;ng nhận l&agrave; di sản thi&ecirc;n nhi&ecirc;n Thế giới năm 1994.&nbsp;<br />
- Động Thi&ecirc;n Cung l&agrave; một trong những động đẹp nhất ở Hạ Long. Vẻ đẹp nguy nga v&agrave; lộng lẫy bởi những lớp thạch nhũ v&agrave; những luồng &aacute;nh s&aacute;ng lung linh.<br />
- Từ tr&ecirc;n t&agrave;u ngắm nh&igrave;n c&aacute;c h&ograve;n đảo lớn nhỏ trong Vịnh Hạ Long: H&ograve;n G&agrave; Chọi, H&ograve;n Lư Hương.&nbsp;<br />
Sau đó đoàn khởi hành đi Ninh Bình tham quan:<br />
-&nbsp; Chùa B&aacute;i Đ&iacute;nh - L&agrave; một quần thể ch&ugrave;a với nhiều kỷ lục Việt Nam như ch&ugrave;a c&oacute; diện t&iacute;ch lớn nhất, tượng phật bằng đồng lớn nhất, nhiều tượng Phật La H&aacute;n nhất&hellip;<br />
Nghỉ đ&ecirc;m tại Ninh B&igrave;nh.<br />
Xem th&ecirc;m &nbsp;<br />
Ng&agrave;y 06<br />
12-07-2020<br />
&nbsp;&nbsp;&nbsp;NINH B&Igrave;NH - TR&Agrave;NG AN - H&Agrave; NỘI - TP.HCM (Ăn s&aacute;ng, trưa)<br />
Trả ph&ograve;ng, xe đưa đo&agrave;n v&agrave;o viếng&nbsp;<br />
- Khu Du Lịch Tr&agrave;ng An:&nbsp;du kh&aacute;ch l&ecirc;n thuyền truyền thống đi tham quan thắng cảnh hệ thống n&uacute;i đ&aacute; v&ocirc;i h&ugrave;ng vĩ v&agrave; c&aacute;c thung lũng ngập nước th&ocirc;ng với nhau bởi c&aacute;c d&ograve;ng suối tạo n&ecirc;n c&aacute;c hang động ngập nước quanh năm. Điểm xuyến trong khh&ocirc;ng gian hoang sơ, tĩnh lặng l&agrave; h&igrave;nh ảnh r&ecirc;u phong, cổ k&iacute;nh của c&aacute;c m&aacute;i đ&igrave;nh, đền, phủ nằm n&eacute;p m&igrave;nh dưới ch&acirc;n c&aacute;c d&atilde;y n&uacute;i cao.<br />
- Du ngoạn 3 điểm t&acirc;m linh Đền Tr&igrave;nh, Đền Trần v&agrave; Phủ Khống, xuy&ecirc;n qua c&aacute;c hang động như: Hang Tối, Hang S&aacute;ng, Hang Nấu Rượu, Hang Ba Giọt,...bằng những chiếc thuyền ch&egrave;o sẽ gi&uacute;p du kh&aacute;ch cảm nhận r&otilde; n&eacute;t phong cảnh hữu t&igrave;nh nơi đ&acirc;y.<br />
Trở về H&agrave; Nội, xe đưa Qu&yacute; kh&aacute;ch ra s&acirc;n bay Nội B&agrave;i đ&aacute;p chuyến bay về Tp.HCM. Chia tay Qu&yacute; kh&aacute;ch v&agrave; kết th&uacute;c chương tr&igrave;nh du lịch tại s&acirc;n bay T&acirc;n Sơn Nhất.<br />
&nbsp;<br />
Lộ tr&igrave;nh tham khảo, cự ly khoảng:<br />
- Ng&agrave;y 01 (301 km):&nbsp;SB Nội B&agrave;i - TP L&agrave;o Cai (262 km) &ndash; Sa Pa (39 km)<br />
- Ng&agrave;y 02&nbsp;(47 km):&nbsp;Sa Pa &ndash; Fansipan Legend (5 km) &ndash; TP L&agrave;o Cai (42 km)<br />
- Ng&agrave;y 03&nbsp;(300 km):&nbsp;Tp L&agrave;o Cai &ndash; H&agrave; Nội (300 km)<br />
- Ng&agrave;y 04&nbsp;(197 km):&nbsp;H&agrave; Nội &ndash; Y&ecirc;n Tử (141 km) - TP Hạ Long (56 km)<br />
- Ng&agrave;y 05&nbsp;(214 km):&nbsp;TP Hạ Long &ndash; Ch&ugrave;a B&aacute;i Đ&iacute;nh (192 km) &ndash; Ninh B&igrave;nh (22 km)<br />
- Ng&agrave;y 06&nbsp;(138 km):&nbsp;Ninh B&igrave;nh &ndash; KDL Tr&agrave;ng An (10 km) &ndash; SB Nội B&agrave;i &nbsp;(128 km)<br />
&nbsp;</p>
', N'08', 1, 1, 1, 1, 6, 0, NULL, NULL, 10)
SET IDENTITY_INSERT [dbo].[tour] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [matour_unique]    Script Date: 7/12/2020 7:50:24 PM ******/
ALTER TABLE [dbo].[tour] ADD  CONSTRAINT [matour_unique] UNIQUE NONCLUSTERED 
(
	[matour] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [idx_matour_unique]    Script Date: 7/12/2020 7:50:24 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [idx_matour_unique] ON [dbo].[tour]
(
	[matour] ASC
)
WHERE ([matour] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[banner]  WITH CHECK ADD  CONSTRAINT [FK_banner_taikhoan] FOREIGN KEY([nguoitao])
REFERENCES [dbo].[taikhoan] ([id])
GO
ALTER TABLE [dbo].[banner] CHECK CONSTRAINT [FK_banner_taikhoan]
GO
ALTER TABLE [dbo].[chitietdattour]  WITH CHECK ADD  CONSTRAINT [FK_chitietdattour_khachhang] FOREIGN KEY([makhach])
REFERENCES [dbo].[khachhang] ([id])
GO
ALTER TABLE [dbo].[chitietdattour] CHECK CONSTRAINT [FK_chitietdattour_khachhang]
GO
ALTER TABLE [dbo].[chitietdattour]  WITH CHECK ADD  CONSTRAINT [FK_chitietdattour_tour] FOREIGN KEY([matour])
REFERENCES [dbo].[tour] ([id])
GO
ALTER TABLE [dbo].[chitietdattour] CHECK CONSTRAINT [FK_chitietdattour_tour]
GO
ALTER TABLE [dbo].[diadiem]  WITH CHECK ADD  CONSTRAINT [FK_diadiem_taikhoan] FOREIGN KEY([nguoitao])
REFERENCES [dbo].[taikhoan] ([id])
GO
ALTER TABLE [dbo].[diadiem] CHECK CONSTRAINT [FK_diadiem_taikhoan]
GO
ALTER TABLE [dbo].[phuongtien]  WITH CHECK ADD  CONSTRAINT [FK_phuongtien_taikhoan] FOREIGN KEY([nguoitao])
REFERENCES [dbo].[taikhoan] ([id])
GO
ALTER TABLE [dbo].[phuongtien] CHECK CONSTRAINT [FK_phuongtien_taikhoan]
GO
ALTER TABLE [dbo].[tour]  WITH CHECK ADD  CONSTRAINT [FK_tour_diadiem] FOREIGN KEY([diadiem_fk])
REFERENCES [dbo].[diadiem] ([id])
GO
ALTER TABLE [dbo].[tour] CHECK CONSTRAINT [FK_tour_diadiem]
GO
ALTER TABLE [dbo].[tour]  WITH CHECK ADD  CONSTRAINT [FK_tour_phuongtien] FOREIGN KEY([phuongtien_fk])
REFERENCES [dbo].[phuongtien] ([id])
GO
ALTER TABLE [dbo].[tour] CHECK CONSTRAINT [FK_tour_phuongtien]
GO
ALTER TABLE [dbo].[tour]  WITH CHECK ADD  CONSTRAINT [FK_tour_taikhoan] FOREIGN KEY([nguoitao_fk])
REFERENCES [dbo].[taikhoan] ([id])
GO
ALTER TABLE [dbo].[tour] CHECK CONSTRAINT [FK_tour_taikhoan]
GO
USE [master]
GO
ALTER DATABASE [webdulich] SET  READ_WRITE 
GO
