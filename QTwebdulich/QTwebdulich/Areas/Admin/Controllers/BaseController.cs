﻿using QTwebdulich.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace QTwebdulich.Areas.Admin.Controllers
{
    public class BaseController: Controller
    {
        private webdulichEntities db = new webdulichEntities();
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var session = Session["AdminUser"];

            int moi = Convert.ToInt32(AdminCommon.TinhTrangDon.Moi);
            List<chitietdattour> ketqua_moi = db.chitietdattours
                .Where(x => x.daxoa != true
                && x.tinhtrangdonhang == moi)
                .ToList();

            int demdonhangmoi = 0;
            if(ketqua_moi.Count() > 0)
            {
                demdonhangmoi = ketqua_moi.Count();
            }
            if (demdonhangmoi > 0)
            {
                Session["demdonhangmoi"] = demdonhangmoi;
            }

            if (session == null)
            {
                filterContext.Result = new RedirectResult("~/Admin");
                return;
            }
        }
    }
}