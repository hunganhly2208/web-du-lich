﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QTwebdulich.Models;
using System.Data.Entity;

namespace QTwebdulich.Areas.Admin.Controllers
{
    public class AccountController : Controller
    {
        private webdulichEntities db = new webdulichEntities();
        // GET: Admin/Account
        public ActionResult Login()
        {
            var session = Session["AdminUser"];
            if (session != null)
            {
                return RedirectToAction("Index" ,"Report");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(taikhoan account)
        {
            taikhoan u = db.taikhoans.Where(m => m.username == account.username && m.password == account.password).FirstOrDefault();
            if (u != null)
            {
                Session["AdminUser"] = u;

                DateTime _now = System.DateTime.Now;
                u.landangnhapcuoi = _now;
                db.taikhoans.Attach(u);
                db.Entry(u).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index", "Report");
            }
            else
            {
                Session["AdminUser"] = null;
                ViewBag.Message = "";
                ViewBag.Message = "Sai tài khoản hoặc mật khẩu";
                return View();
            }
        }
        public ActionResult Logout()
        {
            taikhoan u = new taikhoan();
            u = Session["AdminUser"] as taikhoan;
            try
            {
                if (u != null)
                {
                    u.landangnhapcuoi = DateTime.Now;
                    db.taikhoans.Attach(u);
                    db.Entry(u).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }
            Session["AdminUser"] = null;
            //return RedirectToAction("Login", "Account");
            return Redirect("~/Admin");
        }
    }
}