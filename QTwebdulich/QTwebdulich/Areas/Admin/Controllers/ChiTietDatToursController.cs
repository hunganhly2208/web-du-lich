﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QTwebdulich.Models;
using QTwebdulich.Areas.Admin.Controllers;

namespace QTwebdulich.Areas.Admin.Controllers
{
    public class ChiTietDatToursController : BaseController
    {
        public AdminCommon Common = new AdminCommon();
        private webdulichEntities db = new webdulichEntities();

        // GET: Admin/ChiTietDatTours
        public ActionResult Index(int? tinhtrangdon)
        {
            var chitietdattours = db.chitietdattours.Include(c => c.khachhang).Include(c => c.tour);
            var dahuy = Convert.ToInt32(AdminCommon.TinhTrangDon.DaHuy);
            List<chitietdattour> ketqua = new List<chitietdattour>();
            if (tinhtrangdon == dahuy)
            {
                ketqua = chitietdattours.Where(x => x.tinhtrangdonhang == dahuy && x.daxoa != true).ToList();
            } else
            {
                ketqua = chitietdattours.Where(x => x.tinhtrangdonhang != dahuy && x.daxoa != true).ToList();
            }
            ketqua = ketqua.OrderByDescending(x => x.ngaytao).ToList();
            ViewBag.tinhtrangdon = tinhtrangdon;
            return View(ketqua);
        }

        // GET: Admin/ChiTietDatTours/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            chitietdattour chitietdattour = db.chitietdattours.Find(id);
            if (chitietdattour == null)
            {
                return HttpNotFound();
            }
            return View(chitietdattour);
        }

        // GET: Admin/ChiTietDatTours/Create
        public ActionResult Create()
        {
            ViewBag.makhach = new SelectList(db.khachhangs, "id", "ten");
            ViewBag.matour = new SelectList(db.tours, "id", "ten");
            return View();
        }

        // POST: Admin/ChiTietDatTours/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,matour,makhach,yeucau,hoten,dienthoai,email,ngaytao,ngaycapnhat,hinhthucthanhtoan,giatien,giamgia,daxoa,tinhtrangdonhang")] chitietdattour chitietdattour)
        {
            if (ModelState.IsValid)
            {
                db.chitietdattours.Add(chitietdattour);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.makhach = new SelectList(db.khachhangs, "id", "ten", chitietdattour.makhach);
            ViewBag.matour = new SelectList(db.tours, "id", "ten", chitietdattour.matour);
            return View(chitietdattour);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            chitietdattour chitietdattour = db.chitietdattours.Find(id);
            if (chitietdattour == null)
            {
                return HttpNotFound();
            }
            ViewBag.makhach = new SelectList(db.khachhangs, "id", "ten", chitietdattour.makhach);
            ViewBag.matour = new SelectList(db.tours, "id", "ten", chitietdattour.matour);

            
            var tinhtrangdonhangOptions = Common.listTinhTrangDonhang();
            SelectList listinhtrangdon = new SelectList(tinhtrangdonhangOptions, "id", "ten", chitietdattour.tinhtrangdonhang);
            ViewBag.tinhtrangdonhang = listinhtrangdon;
            return View(chitietdattour);
        }

        // POST: Admin/ChiTietDatTours/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,matour,makhach,yeucau,hoten,dienthoai,email,ngaytao,ngaycapnhat,hinhthucthanhtoan,giatien,giamgia,daxoa,tinhtrangdonhang")] chitietdattour chitietdattour)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chitietdattour).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.makhach = new SelectList(db.khachhangs, "id", "ten", chitietdattour.makhach);
            ViewBag.matour = new SelectList(db.tours, "id", "ten", chitietdattour.matour);
            return View(chitietdattour);
        }


        public ActionResult DaHuy(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            chitietdattour chitietdattour = db.chitietdattours.Find(id);
            if (chitietdattour == null)
            {
                return HttpNotFound();
            }
            ViewBag.makhach = new SelectList(db.khachhangs, "id", "ten", chitietdattour.makhach);
            ViewBag.matour = new SelectList(db.tours, "id", "ten", chitietdattour.matour);

            var tinhtrangdonhangOptions = Common.listTinhTrangDonhang();
            SelectList listinhtrangdon = new SelectList(tinhtrangdonhangOptions, "id", "ten", chitietdattour.tinhtrangdonhang);
            ViewBag.tinhtrangdonhang = listinhtrangdon;
            return View(chitietdattour);
        }

        // POST: Admin/ChiTietDatTours/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DaHuy([Bind(Include = "id,matour,makhach,yeucau,hoten,dienthoai,email,ngaytao,ngaycapnhat,hinhthucthanhtoan,giatien,giamgia,daxoa,tinhtrangdonhang")] chitietdattour chitietdattour)
        {
            if (ModelState.IsValid)
            {
                chitietdattour foundItem = db.chitietdattours.Find(chitietdattour.id);
                foundItem.tinhtrangdonhang = Convert.ToInt32(AdminCommon.TinhTrangDon.Moi);
                db.Entry(foundItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { tinhtrangdon = 4 });
            }
            ViewBag.makhach = new SelectList(db.khachhangs, "id", "ten", chitietdattour.makhach);
            ViewBag.matour = new SelectList(db.tours, "id", "ten", chitietdattour.matour);
            return View(chitietdattour);
        }



        // GET: Admin/ChiTietDatTours/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            chitietdattour chitietdattour = db.chitietdattours.Find(id);
            if (chitietdattour == null)
            {
                return HttpNotFound();
            }
            return View(chitietdattour);
        }

        // POST: Admin/ChiTietDatTours/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            chitietdattour chitietdattour = db.chitietdattours.Find(id);
            db.chitietdattours.Remove(chitietdattour);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
