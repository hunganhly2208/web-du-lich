﻿using System.Web.Mvc;

namespace QTwebdulich.Areas.Admin.Controllers
{
    public class SharedController : Controller
    {
        [ChildActionOnly]
        public ActionResult DemDonHangMoi()
        {
            return PartialView();
        }
    }
}