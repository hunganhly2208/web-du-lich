﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QTwebdulich.Models;

namespace QTwebdulich.Areas.Admin.Controllers
{
    public class DiaDiemsController : BaseController
    {
        private webdulichEntities db = new webdulichEntities();

        // GET: Admin/DiaDiems
        public ActionResult Index()
        {
            var diadiems = db.diadiems.Include(d => d.taikhoan);
            return View(diadiems.ToList());
        }

        // GET: Admin/DiaDiems/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            diadiem diadiem = db.diadiems.Find(id);
            if (diadiem == null)
            {
                return HttpNotFound();
            }
            return View(diadiem);
        }

        // GET: Admin/DiaDiems/Create
        public ActionResult Create()
        {
            ViewBag.nguoitao = new SelectList(db.taikhoans, "id", "username");
            return View();
        }

        // POST: Admin/DiaDiems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,ten,nguoitao,daxoa,ngaytao,ngaycapnhat")] diadiem diadiem)
        {
            if (ModelState.IsValid)
            {
                db.diadiems.Add(diadiem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.nguoitao = new SelectList(db.taikhoans, "id", "username", diadiem.nguoitao);
            return View(diadiem);
        }

        // GET: Admin/DiaDiems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            diadiem diadiem = db.diadiems.Find(id);
            if (diadiem == null)
            {
                return HttpNotFound();
            }
            ViewBag.nguoitao = new SelectList(db.taikhoans, "id", "username", diadiem.nguoitao);
            return View(diadiem);
        }

        // POST: Admin/DiaDiems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,ten,nguoitao,daxoa,ngaytao,ngaycapnhat")] diadiem diadiem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(diadiem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.nguoitao = new SelectList(db.taikhoans, "id", "username", diadiem.nguoitao);
            return View(diadiem);
        }

        // GET: Admin/DiaDiems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            diadiem diadiem = db.diadiems.Find(id);
            if (diadiem == null)
            {
                return HttpNotFound();
            }
            return View(diadiem);
        }

        // POST: Admin/DiaDiems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            diadiem diadiem = db.diadiems.Find(id);
            db.diadiems.Remove(diadiem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
