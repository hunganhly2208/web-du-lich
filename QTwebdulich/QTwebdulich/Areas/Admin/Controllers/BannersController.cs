﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QTwebdulich.Models;

namespace QTwebdulich.Areas.Admin.Controllers
{
    public class BannersController : BaseController
    {
        private webdulichEntities db = new webdulichEntities();

        // GET: Admin/Banners
        // Trang chủ: 1
        // Liên hệ: 2
        // Tour: 3
        // Chi tiết tour: 4
        public ActionResult Index(int? loai)
        {
            if (loai == null)
            {
                loai = 1;
                return RedirectToAction("Index", new { loai = loai });
            }
            var banners = db.banners.Where(x=>x.loai == loai && x.daxoa == false);
            return View(banners.ToList());
        }

        // GET: Admin/Banners/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            banner banner = db.banners.Find(id);
            if (banner == null)
            {
                return HttpNotFound();
            }
            return View(banner);
        }

        // GET: Admin/Banners/Create
        public ActionResult Create(int? loai)
        {
            if (loai == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.nguoitao = new SelectList(db.taikhoans, "id", "username");
            return View();
        }

        // POST: Admin/Banners/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int? loai, [Bind(Include = "id,ten,ngaytao,ngaycapnhat,daxoa,active,nguoitao,hinhanh,loai")] banner banner)
        {
            if (loai == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (ModelState.IsValid)
            {
                banner.loai = loai;
                banner.daxoa = false;
                banner.ngaytao = DateTime.Now;
                banner.ngaycapnhat = DateTime.Now;
                if(banner.active == null)
                {
                    banner.active = true;
                }
                db.banners.Add(banner);
                db.SaveChanges();
                return RedirectToAction("Index", new { loai = loai });
            }

            ViewBag.nguoitao = new SelectList(db.taikhoans, "id", "username", banner.nguoitao);
            return View(banner);
        }

        // GET: Admin/Banners/Edit/5
        public ActionResult Edit(int? id, int? loai)
        {
            if (id == null || loai == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            banner banner = db.banners.Find(id);
            if (banner == null)
            {
                return HttpNotFound();
            }
            ViewBag.nguoitao = new SelectList(db.taikhoans, "id", "username", banner.nguoitao);
            return View(banner);
        }

        // POST: Admin/Banners/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int? loai, [Bind(Include = "id,ten,ngaytao,ngaycapnhat,daxoa,active,nguoitao,hinhanh,loai")] banner banner)
        {
            if (ModelState.IsValid)
            {
                banner.ngaycapnhat = DateTime.Now;
                db.Entry(banner).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { loai = loai });
            }
            ViewBag.nguoitao = new SelectList(db.taikhoans, "id", "username", banner.nguoitao);
            return View(banner);
        }

        // GET: Admin/Banners/Delete/5
        public ActionResult Delete(int? id, int? loai)
        {
            if (id == null || loai == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            banner banner = db.banners.Find(id);
            if (banner == null)
            {
                return HttpNotFound();
            }
            return View(banner);
        }

        // POST: Admin/Banners/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? loai, int id)
        {
            banner banner = db.banners.Find(id);
            banner.ngaycapnhat = DateTime.Now;
            banner.daxoa = true;
            db.Entry(banner).State = EntityState.Modified;
            //db.banners.Remove(banner);
            db.SaveChanges();
            return RedirectToAction("Index", new { loai = loai });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
