﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QTwebdulich.Models;

namespace QTwebdulich.Areas.Admin.Controllers
{
    public class PhuongTiensController : BaseController
    {
        private webdulichEntities db = new webdulichEntities();

        // GET: Admin/PhuongTiens
        public ActionResult Index()
        {
            var phuongtiens = db.phuongtiens.Include(p => p.taikhoan);
            return View(phuongtiens.ToList());
        }

        // GET: Admin/PhuongTiens/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            phuongtien phuongtien = db.phuongtiens.Find(id);
            if (phuongtien == null)
            {
                return HttpNotFound();
            }
            return View(phuongtien);
        }

        // GET: Admin/PhuongTiens/Create
        public ActionResult Create()
        {
            ViewBag.nguoitao = new SelectList(db.taikhoans, "id", "username");
            return View();
        }

        // POST: Admin/PhuongTiens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,ten,nguoitao,daxoa,ngaytao,ngaycapnhat")] phuongtien phuongtien)
        {
            if (ModelState.IsValid)
            {
                db.phuongtiens.Add(phuongtien);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.nguoitao = new SelectList(db.taikhoans, "id", "username", phuongtien.nguoitao);
            return View(phuongtien);
        }

        // GET: Admin/PhuongTiens/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            phuongtien phuongtien = db.phuongtiens.Find(id);
            if (phuongtien == null)
            {
                return HttpNotFound();
            }
            ViewBag.nguoitao = new SelectList(db.taikhoans, "id", "username", phuongtien.nguoitao);
            return View(phuongtien);
        }

        // POST: Admin/PhuongTiens/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,ten,nguoitao,daxoa,ngaytao,ngaycapnhat")] phuongtien phuongtien)
        {
            if (ModelState.IsValid)
            {
                db.Entry(phuongtien).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.nguoitao = new SelectList(db.taikhoans, "id", "username", phuongtien.nguoitao);
            return View(phuongtien);
        }

        // GET: Admin/PhuongTiens/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            phuongtien phuongtien = db.phuongtiens.Find(id);
            if (phuongtien == null)
            {
                return HttpNotFound();
            }
            return View(phuongtien);
        }

        // POST: Admin/PhuongTiens/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            phuongtien phuongtien = db.phuongtiens.Find(id);
            db.phuongtiens.Remove(phuongtien);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
