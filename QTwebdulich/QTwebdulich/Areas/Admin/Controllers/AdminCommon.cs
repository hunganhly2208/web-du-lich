﻿using QTwebdulich.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace QTwebdulich.Areas.Admin.Controllers
{
    public class AdminCommon
    {
        public enum TinhTrangDon {
            Moi = 1,
            DangXuLy = 2,
            HoanThanh = 3,
            DaHuy = 4,
        }; 
        public class TinhTrangDonHang
        {
            public int id { get; set; }
            public string ten { get; set; }
        }
 
        public webdulichEntities db = new webdulichEntities();
        // 1: moi
        // 2: dang xu ly
        // 3: hoan thanh
        // 4: huy
        public List<TinhTrangDonHang> listTinhTrangDonhang()
        {
            List<TinhTrangDonHang> list = new List<TinhTrangDonHang>();
            TinhTrangDonHang tinhtrangdon1 = new TinhTrangDonHang();
            tinhtrangdon1.id = 1;
            tinhtrangdon1.ten = "Đơn hàng mới";
            list.Add(tinhtrangdon1);
            TinhTrangDonHang tinhtrangdon2 = new TinhTrangDonHang();
            tinhtrangdon2.id = 2;
            tinhtrangdon2.ten = "Đang xử lý";
            list.Add(tinhtrangdon2);
            TinhTrangDonHang tinhtrangdon3 = new TinhTrangDonHang();
            tinhtrangdon3.id = 3;
            tinhtrangdon3.ten = "Hoàn thành";
            list.Add(tinhtrangdon3);
            TinhTrangDonHang tinhtrangdon4 = new TinhTrangDonHang();
            tinhtrangdon4.id = 4;
            tinhtrangdon4.ten = "Hủy";
            list.Add(tinhtrangdon4);
            return list;
        }
    }
}