﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QTwebdulich.Models;

namespace QTwebdulich.Areas.Admin.Controllers
{
    public class ToursController : BaseController
    {
        private webdulichEntities db = new webdulichEntities();

        // GET: Admin/Tours
        public ActionResult Index()
        {
            var tours = db.tours.Include(t => t.diadiem).Include(t => t.phuongtien).Include(t => t.taikhoan);
            return View(tours.ToList());
        }

        // GET: Admin/Tours/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tour tour = db.tours.Find(id);
            if (tour == null)
            {
                return HttpNotFound();
            }
            return View(tour);
        }

        // GET: Admin/Tours/Create
        public ActionResult Create()
        {
            ViewBag.diadiem_fk = new SelectList(db.diadiems, "id", "ten");
            ViewBag.phuongtien_fk = new SelectList(db.phuongtiens, "id", "ten");
            ViewBag.nguoitao_fk = new SelectList(db.taikhoans, "id", "username");
            return View();
        }

        // POST: Admin/Tours/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "id,ten,hinhanh,thoigian,gia,tomtat,chitiet,matour,phuongtien_fk,hienthi,noibat,nguoitao_fk,diadiem_fk,daxoa,ngaytao,ngaycapnhat,giamgia")] tour tour)
        {
            if (ModelState.IsValid)
            {
                tour.daxoa = false;
                db.tours.Add(tour);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.diadiem_fk = new SelectList(db.diadiems, "id", "ten", tour.diadiem_fk);
            ViewBag.phuongtien_fk = new SelectList(db.phuongtiens, "id", "ten", tour.phuongtien_fk);
            ViewBag.nguoitao_fk = new SelectList(db.taikhoans, "id", "username", tour.nguoitao_fk);
            return View(tour);
        }

        // GET: Admin/Tours/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tour tour = db.tours.Find(id);
            if (tour == null)
            {
                return HttpNotFound();
            }
            ViewBag.diadiem_fk = new SelectList(db.diadiems, "id", "ten", tour.diadiem_fk);
            ViewBag.phuongtien_fk = new SelectList(db.phuongtiens, "id", "ten", tour.phuongtien_fk);
            ViewBag.nguoitao_fk = new SelectList(db.taikhoans, "id", "username", tour.nguoitao_fk);
            return View(tour);
        }

        // POST: Admin/Tours/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "id,ten,hinhanh,thoigian,gia,tomtat,chitiet,matour,phuongtien_fk,hienthi,noibat,nguoitao_fk,diadiem_fk,daxoa,ngaytao,ngaycapnhat,giamgia")] tour tour)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tour).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.diadiem_fk = new SelectList(db.diadiems, "id", "ten", tour.diadiem_fk);
            ViewBag.phuongtien_fk = new SelectList(db.phuongtiens, "id", "ten", tour.phuongtien_fk);
            ViewBag.nguoitao_fk = new SelectList(db.taikhoans, "id", "username", tour.nguoitao_fk);
            return View(tour);
        }

        // GET: Admin/Tours/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tour tour = db.tours.Find(id);
            if (tour == null)
            {
                return HttpNotFound();
            }
            return View(tour);
        }

        // POST: Admin/Tours/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tour tour = db.tours.Find(id);
            tour.daxoa = true;
            db.Entry(tour).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
