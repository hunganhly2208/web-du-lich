﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QTwebdulich.Models;

namespace QTwebdulich.Areas.Admin.Controllers
{
    public class LienHeController : Controller
    {
        private webdulichEntities db = new webdulichEntities();

        // GET: Admin/LienHe
        public ActionResult Index()
        {
            return View(db.lienhes.ToList());
        }

        // GET: Admin/LienHe/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lienhe lienhe = db.lienhes.Find(id);
            if (lienhe == null)
            {
                return HttpNotFound();
            }
            return View(lienhe);
        }

        // GET: Admin/LienHe/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/LienHe/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,hoten,diachi,email,phone,tieude,yeucau,ngaytao")] lienhe lienhe)
        {
            if (ModelState.IsValid)
            {
                db.lienhes.Add(lienhe);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lienhe);
        }

        // GET: Admin/LienHe/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lienhe lienhe = db.lienhes.Find(id);
            if (lienhe == null)
            {
                return HttpNotFound();
            }
            return View(lienhe);
        }

        // POST: Admin/LienHe/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,hoten,diachi,email,phone,tieude,yeucau,ngaytao")] lienhe lienhe)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lienhe).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lienhe);
        }

        // GET: Admin/LienHe/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lienhe lienhe = db.lienhes.Find(id);
            if (lienhe == null)
            {
                return HttpNotFound();
            }
            return View(lienhe);
        }

        // POST: Admin/LienHe/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            lienhe lienhe = db.lienhes.Find(id);
            db.lienhes.Remove(lienhe);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
