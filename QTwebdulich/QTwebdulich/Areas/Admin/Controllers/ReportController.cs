﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QTwebdulich.Models;

namespace QTwebdulich.Areas.Admin.Controllers
{
    public class ReportController : BaseController
    {
        private webdulichEntities db = new webdulichEntities();

        public DateTime AbsoluteStart(DateTime dateTime)
        {
            return dateTime.Date;
        }

        /// <summary>
        /// Gets the 11:59:59 instance of a DateTime
        /// </summary>
        public DateTime AbsoluteEnd(DateTime dateTime)
        {
            return AbsoluteStart(dateTime).AddDays(1).AddTicks(-1);
        }

        // GET: Admin/Report
        public ActionResult Index()
        {
            string url = Request.RawUrl;
            string query = Request.Url.Query;
            string from = Request.QueryString["from"];
            string to = Request.QueryString["to"];


            // Thong ke ngay hom nay (mac dinh)
            int dondukien = 0, giatridukien = 0, donchot = 0, giatrichot = 0, donhuy = 0, giatrihuy = 0;
            List<chitietdattour> thongketoanbo = new List<chitietdattour>();
            try
            {
                DateTime today = DateTime.Today;
                var daungay = AbsoluteStart(today);
                var cuoingay = AbsoluteEnd(today);
                if(from != null)
                {
                    daungay = AbsoluteStart(DateTime.Parse(from));
                }
                if (from != null)
                {
                    cuoingay = AbsoluteEnd(DateTime.Parse(to));
                }

                thongketoanbo = db.chitietdattours
                    .Where(x => x.ngaytao >= daungay
                    && x.ngaytao <= cuoingay
                    && x.daxoa != true)
                    .OrderByDescending(x => x.ngaytao).ToList();

                var ketqua_dondukien = db.chitietdattours
                    .Where(x => x.ngaytao >= daungay && x.ngaytao <= cuoingay && x.daxoa != true).ToList();
                if(ketqua_dondukien.Count > 0)
                {
                    dondukien = ketqua_dondukien.Count();
                    
                    foreach (chitietdattour item_dondukien in ketqua_dondukien)
                    {
                        int tonggia = Convert.ToInt32(item_dondukien.giatien);
                        int giamgia = tonggia * Convert.ToInt32(item_dondukien.giamgia) / 100;
                        int giasanpham = tonggia - giamgia;
                        giatridukien = giatridukien + giasanpham;
                    }
                }

                int hoanthanh = Convert.ToInt32(AdminCommon.TinhTrangDon.HoanThanh);
                var ketqua_donchot = db.chitietdattours
                    .Where(x => x.ngaytao >= daungay 
                    && x.ngaytao <= cuoingay 
                    && x.daxoa != true 
                    && x.tinhtrangdonhang == hoanthanh)
                    .ToList();
                if (ketqua_donchot.Count > 0)
                {
                    donchot = ketqua_donchot.Count();

                    foreach (chitietdattour item_donchot in ketqua_donchot)
                    {
                        int tonggia = Convert.ToInt32(item_donchot.giatien);
                        int giamgia = tonggia * Convert.ToInt32(item_donchot.giamgia) / 100;
                        int giasanpham = tonggia - giamgia;
                        giatrichot = giatrichot + giasanpham;
                    }
                }

                int dahuy = Convert.ToInt32(AdminCommon.TinhTrangDon.DaHuy);
                var ketqua_donhuy = db.chitietdattours
                    .Where(x => x.ngaytao >= daungay 
                    && x.ngaytao <= cuoingay
                    && x.daxoa != true
                    && x.tinhtrangdonhang == dahuy)
                    .ToList();
                if (ketqua_donhuy.Count > 0)
                {
                    donhuy = ketqua_donhuy.Count();

                    foreach (chitietdattour item_donhuy in ketqua_donhuy)
                    {
                        int tonggia = Convert.ToInt32(item_donhuy.giatien);
                        int giamgia = tonggia * Convert.ToInt32(item_donhuy.giamgia) / 100;
                        int giasanpham = tonggia - giamgia;
                        giatrihuy = giatrihuy + giasanpham;
                    }
                }
            }
            catch (Exception ex)
            { }
            ViewBag.dondukien = dondukien;
            ViewBag.giatridukien = giatridukien;
            ViewBag.donchot = donchot;
            ViewBag.giatrichot = giatrichot;
            ViewBag.donhuy = donhuy;
            ViewBag.giatrihuy = giatrihuy;
            return View(thongketoanbo);
        }

        [HttpPost]
        public ActionResult Index(string from, string to)
        {
            return Redirect("~/Admin/Report/Index?from="+ from + "&to="+ to);
        }
    }
}