﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QTwebdulich.Startup))]
namespace QTwebdulich
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
