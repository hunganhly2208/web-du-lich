﻿using QTwebdulich.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using QTwebdulich.Controllers;
using QTwebdulich.Areas.Admin.Controllers;
using System.Net;

namespace QTwebdulich.Controllers
{
    public class CustomerController : Controller
    {
        private webdulichEntities db = new webdulichEntities();
        public Helper Helper = new Helper();
        public ActionResult Index()
        {
            var session = Session["ClientUser"];
            if (session == null)
            {
                return Redirect("/");
            }

            khachhang sessionKhachhang = session as khachhang;
            return View(sessionKhachhang);
        }


        public ActionResult ChangePassword()
        {
            var session = Session["ClientUser"];
            if (session == null)
            {
                return Redirect("/");
            }

            khachhang sessionKhachhang = session as khachhang;
            return View(sessionKhachhang);
        }

        public class ClassPassword
        {
            public string password_cu { get; set; }
            public string password_moi { get; set; }
        }

        [HttpPost]
        public ActionResult ChangePassword(ClassPassword thongtinmatkhau)
        {
            try
            {
                var session = Session["ClientUser"];
                if (session == null)
                {
                    return Redirect("/");
                }

                khachhang sessionKhachhang = session as khachhang;

                khachhang khachhang = db.khachhangs.FirstOrDefault(x => x.id == sessionKhachhang.id);

                if (thongtinmatkhau.password_cu == thongtinmatkhau.password_moi)
                {
                    ViewBag.message = "Trùng mật khẩu cũ";
                    return View();
                }
                if (thongtinmatkhau.password_cu != khachhang.password)
                {
                    ViewBag.message = "Mật khẩu cũ không chính xác";
                    return View();
                }

                
                khachhang.ngaycapnhat = DateTime.Now;
                khachhang.password = thongtinmatkhau.password_moi;
                db.Entry(khachhang).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception) { }

            return Redirect("~/Customer");
        }

        public ActionResult Order()
        {
            var session = Session["ClientUser"];
            if (session == null)
            {
                return Redirect("/");
            }

            khachhang sessionKhachhang = session as khachhang;
            var chitietdattours = db.chitietdattours
                .Include(c => c.tour)
                .Where(s => s.makhach == sessionKhachhang.id && s.daxoa != true)
                .OrderByDescending(x => x.ngaytao).ToList();
            return View(chitietdattours);
        }

        [HttpPost]
        public ActionResult Update(khachhang account)
        {
            try
            {
                var session = Session["ClientUser"];
                if (session == null)
                {
                    return Redirect("/");
                }

                khachhang sessionKhachhang = session as khachhang;


                khachhang khachhang = db.khachhangs.FirstOrDefault(x => x.id == sessionKhachhang.id);
                khachhang.ngaycapnhat = DateTime.Now;
                khachhang.ten = account.ten;
                khachhang.dienthoai = account.dienthoai;
                db.Entry(khachhang).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception) {}
            
            return Redirect("~/Customer");
        }

        public ActionResult Cancel(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var session = Session["ClientUser"];
            if (session == null)
            {
                return Redirect("/");
            }

            var foundOrder = db.chitietdattours.FirstOrDefault(x=>x.id == id);
            var tinhtrangmoi = Convert.ToInt32(AdminCommon.TinhTrangDon.Moi);
            if (foundOrder != null && foundOrder.tinhtrangdonhang == tinhtrangmoi)
            {
                // Huy don
                foundOrder.tinhtrangdonhang = Convert.ToInt32(AdminCommon.TinhTrangDon.DaHuy);
                foundOrder.ngaycapnhat = DateTime.Now;
                db.Entry(foundOrder).State = EntityState.Modified;
                db.SaveChanges();
            }
            return Redirect("~/Customer/Order");
        }


        public ActionResult Login()
        {
            var session = Session["ClientUser"];
            if (session != null)
            {
                return Redirect("/");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Login(khachhang account)
        {
            khachhang u = db.khachhangs
                .Where(m => m.username == account.username 
                    && m.password == account.password 
                    && m.daxoa != true 
                    && m.active == true)
                .FirstOrDefault();
            if (u != null)
            {
                Session["ClientUser"] = u;
                Session["ClientUserName"] = u.ten;

                return Redirect("/");
            }
            else
            {
                Session["ClientUser"] = null;
                ViewBag.Message = "";
                ViewBag.Message = "Sai tài khoản hoặc mật khẩu";
                return View();
            }
        }

        public ActionResult Register()
        {
            var session = Session["ClientUser"];
            if (session != null)
            {
                return Redirect("~/");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Register([Bind(Include = "ten,dienthoai,email,username,password")] khachhang account)
        {
            try
            {
                var foundUserName = db.khachhangs.Where(s => s.username == account.username).ToList();
                if(foundUserName.Count > 0)
                {
                    ViewBag.foundUserName = "Trùng tên đăng nhập";
                    return View();
                }

                var foundEmail = db.khachhangs.Where(s => s.email == account.email).ToList();
                if (foundEmail.Count > 0)
                {
                    ViewBag.foundEmail = "Trùng email";
                    return View();
                }

                var foundSdt = db.khachhangs.Where(s => s.dienthoai == account.dienthoai).ToList();
                if (foundSdt.Count > 0)
                {
                    ViewBag.foundSdt = "Trùng số điện thoại";
                    return View();
                }


                var khachhang = new khachhang();
                khachhang.ten = account.ten;
                khachhang.dienthoai = account.dienthoai;
                khachhang.email = account.email;
                khachhang.username = account.username;
                khachhang.password = account.password;
                khachhang.active = true;
                khachhang.daxoa = false;
                khachhang.ngaytao = DateTime.Now;
                khachhang.ngaycapnhat = DateTime.Now;
                db.khachhangs.Add(khachhang);
                db.SaveChanges();

                Session["ClientUser"] = khachhang;
                Session["ClientUserName"] = khachhang.ten;
                return Redirect("/");
            }
            catch (Exception ex)
            {
                return View();
            }
            
        }

        public ActionResult Logout()
        {
            Session["ClientUser"] = null;
            Session["ClientUserName"] = null;
            return Redirect("~/");
        }
    }
}