﻿using QTwebdulich.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QTwebdulich.Controllers;

namespace QTwebdulich.Controllers
{
    public class PurchaseController : Controller
    {
        private webdulichEntities db = new webdulichEntities();
        private Helper Helper = new Helper();
        // GET: Purchase
        public ActionResult Index(string tour)
        {
            try
            {
                
                if(Session["ClientUser"] != null)
                {
                    var user = new khachhang();
                    user = Session["ClientUser"] as khachhang;
                    ViewBag.tenKH = user.ten;
                    ViewBag.dienthoaiKH = user.dienthoai;
                    ViewBag.mailKH = user.email;
                }

                if (tour == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                int idTour = Convert.ToInt32(tour);
                tour tourObj = db.tours.FirstOrDefault(x => x.id == idTour && x.daxoa != true);
                if (tourObj == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                };
                ViewBag.tour = tourObj;
                int giaban = 0;
                giaban = Helper.LayGiaBanSauGiamGia(tourObj.gia, tourObj.giamgia);
                ViewBag.giaban = giaban;
                return View();
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
        }

        [HttpPost]
        public ActionResult DatTour(string hoten, string sdt, string email, string yckhac, string hinhthucthanhtoan, string idTour)
        {
            try
            {
                int _hinhthucthanhtoan = Convert.ToInt32(hinhthucthanhtoan);
                // hinhthucthanhtoan:
                // 1: den van phong
                // 2: tai nha
                // 3: chuyen khoan
                // khac: failure
                if (_hinhthucthanhtoan != 1 && _hinhthucthanhtoan != 2 && _hinhthucthanhtoan != 3)
                {
                    return RedirectToAction("Failure", "Purchase");
                };
                chitietdattour donhang = new chitietdattour();
                donhang.hoten = hoten;
                donhang.dienthoai = sdt;
                donhang.email = email;
                donhang.yeucau = yckhac;
                donhang.hinhthucthanhtoan = _hinhthucthanhtoan;

                //Kiem tra neu co dang nhap thi luu them thong tin khach hang
                if (Session["ClientUser"] != null)
                {
                    var user = new khachhang();
                    user = Session["ClientUser"] as khachhang;
                    donhang.makhach = user.id;
                }

                int _idTour = Convert.ToInt32(idTour);
                tour tour = db.tours.FirstOrDefault(x => x.id == _idTour && x.daxoa != true);
                if (tour == null)
                {
                    return RedirectToAction("Failure", "Purchase");
                }
                donhang.matour = tour.id;
                donhang.ngaytao = DateTime.Now;
                donhang.ngaycapnhat = DateTime.Now;
                donhang.giatien = tour.gia;
                donhang.daxoa = false;
                donhang.giamgia = tour.giamgia != 0 ? tour.giamgia : 0;
                // tinhtrangdonhang:
                // 1: moi
                // 2: dang xu ly
                // 3: hoan thanh
                // 4: huy
                // khac: failure
                donhang.tinhtrangdonhang = 1;

                db.chitietdattours.Add(donhang);
                db.SaveChanges();

                string msg = "Đặt tour thành công. Xin cảm ơn quí khách đã lựa chọn chúng tôi.";
                string tieude = "Xác nhận đơn đặt hàng - Web Quang Tùng";
                //Gui mail
                Helper.GuiMail(email, msg, tieude);

                return RedirectToAction("Success", "Purchase");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Failure", "Purchase");
                throw;
            }

        }

        public ActionResult Success()
        {
            return View();
        }

        public ActionResult Failure()
        {
            return View();
        }
    }
}