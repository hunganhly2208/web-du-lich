﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QTwebdulich.Controllers
{
    public class SharedController : Controller
    {
        [ChildActionOnly]
        public ActionResult SearchForm()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult SearchForm(string tentour)
        {
            string urlRedirect = "~/Tours/Search?tour=" + tentour;
            return Redirect(urlRedirect);
        }
    }
}