﻿using QTwebdulich.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using QTwebdulich.Controllers;

namespace QTwebdulich.Controllers
{   
    public class HomeController : Controller
    {
        private webdulichEntities db = new webdulichEntities();
        public Helper Helper = new Helper();
        public ActionResult Index()
        {
            // lay list những tour nổi bật = true, đã xóa = false
            List<tour> tours = db.tours
                .Include(t => t.diadiem)
                .Include(t => t.phuongtien)
                .Include(t => t.taikhoan)
                .Where(x=>x.noibat == true && x.daxoa == false && x.hienthi == true).ToList();
            ViewBag.tourNoiBat = tours;

            // Loại = 1: Trang chủ
            banner banner = db.banners.FirstOrDefault(x => x.loai == 1 && x.active == true && x.daxoa == false);
            ViewBag.banner = "";
            if (banner != null)
            {
                ViewBag.banner = banner.hinhanh;
            }
            return View();
        }

        public ActionResult Contact()
        {
            // Loại = 2: Liên hệ
            banner banner = db.banners.FirstOrDefault(x => x.loai == 2 && x.active == true && x.daxoa == false);
            if (banner != null)
            {
                ViewBag.banner = banner.hinhanh;
            }
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult Contact(string hoten, string diachi, string email, string phone, string tieude, string yeucau)
        {
            lienhe lienhe = new lienhe();
            lienhe.hoten = hoten;
            lienhe.diachi = diachi;
            lienhe.email = email;
            lienhe.phone = phone;
            lienhe.tieude = tieude;
            lienhe.yeucau = yeucau;
            lienhe.ngaytao = DateTime.Now;

            db.lienhes.Add(lienhe);
            db.SaveChanges();

            try
            {
                string msg = tieude;
                string _tieude = "Web Quang Tùng - Bạn có 1 mail liên hệ từ khách hàng";
                //Gui mail
                string _email = "tranquangtung2502@gmail.com";
                Helper.GuiMail(_email, msg, _tieude);
            }
            catch (Exception ex)
            {}
            return View();
        }
    }
}