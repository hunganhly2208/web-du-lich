﻿using QTwebdulich.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using System.Net;
using QTwebdulich.Controllers;
using System.Text.RegularExpressions;
using System.Globalization;

namespace QTwebdulich.Controllers
{
    public class ToursController : Controller
    {
        // GET: Tours
        private webdulichEntities db = new webdulichEntities();
        public Helper Helper = new Helper();
        public ActionResult Index()
        {

            // Loại = 3: Tour
            banner banner = db.banners.FirstOrDefault(x => x.loai == 3 && x.active == true && x.daxoa == false);
            ViewBag.banner = "";
            if (banner != null)
            {
                ViewBag.banner = banner.hinhanh;
            }
            // lay list những tour nổi bật = true, đã xóa = false
            List<tour> toursNoiBat = db.tours
                    .Include(t => t.diadiem)
                    .Include(t => t.phuongtien)
                    .Include(t => t.taikhoan)
                    .Where(x => x.noibat == true && x.daxoa == false && x.hienthi == true).ToList();
            ViewBag.tourNoiBat = toursNoiBat;

            List<tour> toursThuong = db.tours
                    .Include(t => t.diadiem)
                    .Include(t => t.phuongtien)
                    .Include(t => t.taikhoan)
                    .Where(x => x.daxoa == false && x.hienthi == true).ToList();
            ViewBag.toursThuong = toursThuong;
            return View();
        }

        public ActionResult Detail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tour tour = db.tours.FirstOrDefault(x => x.id == id && x.daxoa != true);
            if(tour == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            };
            ViewBag.tour = tour;
            // Loại = 4: Chi tiết tour
            banner banner = db.banners.FirstOrDefault(x => x.loai == 4 && x.active == true && x.daxoa == false);
            ViewBag.banner = "";
            if (banner != null)
            {
                ViewBag.banner = banner.hinhanh;
            }

            //list tour noi bat sidebar
            List<tour> toursNoiBat = db.tours
                    .Include(t => t.diadiem)
                    .Include(t => t.phuongtien)
                    .Include(t => t.taikhoan)
                    .Where(x => x.noibat == true && x.daxoa == false && x.hienthi == true)
                    .Take(6)
                    .ToList();
            ViewBag.tourNoiBat = toursNoiBat;


            return View(tour);
        }

        // http://dotnet.edu.vn/ChuyenMuc/Ham-chuyen-doi-chuoi-ky-tu-co-dau-thanh-khong-dau-CNet-184.aspx
        public static string ConvertToUnSign(string text)
        {
            for (int i = 33; i < 48; i++)
            {
                text = text.Replace(((char)i).ToString(), "");
            }

            for (int i = 58; i < 65; i++)
            {
                text = text.Replace(((char)i).ToString(), "");
            }

            for (int i = 91; i < 97; i++)
            {
                text = text.Replace(((char)i).ToString(), "");
            }
            for (int i = 123; i < 127; i++)
            {
                text = text.Replace(((char)i).ToString(), "");
            }
            text = text.Replace(" ", "-");
            Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string strFormD = text.Normalize(System.Text.NormalizationForm.FormD);
            string result = regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
            result = result.Replace("-", " ");
            return result;
        }

        public ActionResult Search(string tour)
        {
            // Creates a TextInfo based on the "en-US" culture.
            TextInfo myTI = new CultureInfo("en-US", false).TextInfo;

            string tenUppercase_title = myTI.ToTitleCase(tour); // đà lạt -> Đà Lạt
            string tenUppercase = tour.ToUpper(); // đà lạt -> ĐÀ LẠT
            string tenLowercase = tour.ToLower(); // Đà LạT -> đà lạt

            string tenKhongDau = ConvertToUnSign(tour);
            string tenKhongDau_uppercase = tenKhongDau.ToUpper();
            string tenKhongDau_lowercase = tenKhongDau.ToLower();
            string tenKhongDau_title = myTI.ToTitleCase(tenKhongDau); // da Lat -> Da Lat

            banner banner = db.banners.FirstOrDefault(x => x.loai == 3 && x.active == true && x.daxoa == false);
            ViewBag.banner = "";
            if (banner != null)
            {
                ViewBag.banner = banner.hinhanh;
            }
            
            List<tour> toursThuong = db.tours
                    .Include(t => t.diadiem)
                    .Include(t => t.phuongtien)
                    .Include(t => t.taikhoan)
                    .Where(
                        x => x.daxoa == false 
                        && x.hienthi == true 
                        && 
                        (
                            x.ten.Contains(tour) 
                            || x.ten.Contains(tenKhongDau)
                            || x.ten.Contains(tenUppercase_title) 
                            || x.ten.Contains(tenUppercase) 
                            || x.ten.Contains(tenLowercase) 
                            || x.ten.Contains(tenKhongDau_uppercase) 
                            || x.ten.Contains(tenKhongDau_lowercase) 
                            || x.ten.Contains(tenKhongDau_title)
                        )
                    )
                    .ToList();
            ViewBag.toursThuong = toursThuong;
            return View();
        }
    }
}