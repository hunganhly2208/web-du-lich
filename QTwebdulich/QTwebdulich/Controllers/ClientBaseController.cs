﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace QTwebdulich.Controllers
{
    public class ClientBaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var session = Session["ClientUser"];
            if (session == null)
            {
                filterContext.Result = new RedirectResult("~/");
                return;
            }
        }
    }
}