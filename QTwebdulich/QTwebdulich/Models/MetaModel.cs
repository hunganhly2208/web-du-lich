﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QTwebdulich.Models
{
    #region Banner
    [MetadataType(typeof(bannerMetaData))]
    public partial class banner
    { }
    public partial class bannerMetaData
    {
        public int id { get; set; }
        public string ten { get; set; }
        public Nullable<System.DateTime> ngaytao { get; set; }
        public Nullable<System.DateTime> ngaycapnhat { get; set; }
        public Nullable<bool> daxoa { get; set; }
        public Nullable<bool> active { get; set; }
        public Nullable<int> nguoitao { get; set; }
        public string hinhanh { get; set; }
        public Nullable<int> loai { get; set; }

        public virtual taikhoan taikhoan { get; set; }
    }
    #endregion

    #region Chi tiết đặt tour
    [MetadataType(typeof(chitietdattourMetaData))]
    public partial class chitietdattour
    { }
    public partial class chitietdattourMetaData
    {
        public int id { get; set; }
        public Nullable<int> matour { get; set; }
        public Nullable<int> makhach { get; set; }
        [Display(Name = "Yêu cầu")]
        public string yeucau { get; set; }
        public string hoten { get; set; }
        public string dienthoai { get; set; }
        public string email { get; set; }
        public Nullable<System.DateTime> ngaytao { get; set; }
        public Nullable<System.DateTime> ngaycapnhat { get; set; }
        public Nullable<int> hinhthucthanhtoan { get; set; }
        public Nullable<int> giatien { get; set; }
        public Nullable<int> giamgia { get; set; }
        public Nullable<bool> daxoa { get; set; }
        public Nullable<int> tinhtrangdonhang { get; set; }

        public virtual khachhang khachhang { get; set; }
        public virtual tour tour { get; set; }
    }
    #endregion

    #region Địa điểm
    [MetadataType(typeof(diadiemMetaData))]
    public partial class diadiem
    { }
    public partial class diadiemMetaData
    {
        public int id { get; set; }
        public string ten { get; set; }
        public Nullable<int> nguoitao { get; set; }
        public Nullable<bool> daxoa { get; set; }
        public Nullable<System.DateTime> ngaytao { get; set; }
        public Nullable<System.DateTime> ngaycapnhat { get; set; }

        public virtual taikhoan taikhoan { get; set; }
        public virtual ICollection<tour> tours { get; set; }
    }
    #endregion

    #region Khách hàng
    [MetadataType(typeof(khachhangMetaData))]
    public partial class khachhang
    { }
    public partial class khachhangMetaData
    {
        public int id { get; set; }
        public string ten { get; set; }
        public string dienthoai { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public Nullable<System.DateTime> ngaytao { get; set; }
        public Nullable<System.DateTime> ngaycapnhat { get; set; }
        public Nullable<bool> daxoa { get; set; }
        public Nullable<bool> active { get; set; }

        public virtual ICollection<chitietdattour> chitietdattours { get; set; }
    }
    #endregion

    #region Phương tiện
    [MetadataType(typeof(phuongtienMetaData))]
    public partial class phuongtien
    { }
    public partial class phuongtienMetaData
    {
        public int id { get; set; }
        public string ten { get; set; }
        public Nullable<int> nguoitao { get; set; }
        public Nullable<bool> daxoa { get; set; }
        public Nullable<System.DateTime> ngaytao { get; set; }
        public Nullable<System.DateTime> ngaycapnhat { get; set; }

        public virtual taikhoan taikhoan { get; set; }
        public virtual ICollection<tour> tours { get; set; }
    }
    #endregion

    #region Tài khoản
    [MetadataType(typeof(taikhoanMetaData))]
    public partial class taikhoan
    { }
    public partial class taikhoanMetaData
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public Nullable<System.DateTime> ngaytao { get; set; }
        public Nullable<System.DateTime> landangnhapcuoi { get; set; }
        public string hoten { get; set; }
        public Nullable<bool> gioitinh { get; set; }
        public string ghichu { get; set; }
        public Nullable<int> quyen { get; set; }
        public Nullable<bool> daxoa { get; set; }

        public virtual ICollection<diadiem> diadiems { get; set; }
        public virtual ICollection<phuongtien> phuongtiens { get; set; }
        public virtual ICollection<tour> tours { get; set; }
        public virtual ICollection<banner> banners { get; set; }
    }
    #endregion

    #region Tour
    [MetadataType(typeof(tourMetaData))]
    public partial class tour
    { }
    public partial class tourMetaData
    {
        public int id { get; set; }
        public string ten { get; set; }
        public string hinhanh { get; set; }
        public Nullable<System.DateTime> thoigian { get; set; }
        public Nullable<int> gia { get; set; }
        public string tomtat { get; set; }
        public string chitiet { get; set; }
        public string matour { get; set; }
        public Nullable<int> phuongtien_fk { get; set; }
        public Nullable<bool> hienthi { get; set; }
        public Nullable<bool> noibat { get; set; }
        public Nullable<int> nguoitao_fk { get; set; }
        public Nullable<int> diadiem_fk { get; set; }
        public Nullable<bool> daxoa { get; set; }
        public Nullable<System.DateTime> ngaytao { get; set; }
        public Nullable<System.DateTime> ngaycapnhat { get; set; }
        public Nullable<int> giamgia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<chitietdattour> chitietdattours { get; set; }
        public virtual diadiem diadiem { get; set; }
        public virtual phuongtien phuongtien { get; set; }
        public virtual taikhoan taikhoan { get; set; }
    }
    #endregion
}